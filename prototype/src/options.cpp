#include "options.hpp"
#include <cxxopts.hpp>

using namespace std;

namespace iot
{
    Options::Options()
        : port{consts::default_port}
        , mock{false}
        , repl{std::nullopt}
    {}

    Options get_options(int argc, char** argv)
    {
        Options options;
        cxxopts::Options o{"srp", "IOD internal server prototype"};

        o.add_options()
            ("h,help", "this help")
            ("repl", "run REPL on database file", cxxopts::value<string>())
//            ("mock", "simulate IOT (DOES NOTHING YET)")
            ("port", "port to run server on",     cxxopts::value<int>())
            ;
        
        auto r = o.parse(argc, argv);

        if (r.count("help"))
        {
            std::cout << o.help() << "\n";
            std::exit(0);
        }

//        if (r.count("mock"))
//        {
//            options.mock = true;
//        }
        
        if (r.count("repl"))
        {
            options.repl = Path{r["repl"].as<string>()};
        }
        
        if (r.count("port"))
        {
            options.port = r["port"].as<int>();
        }

        return options;
    }
}
