#include "resource.hpp"

using namespace srv;
using namespace std;

namespace iot
{
    http_response const Resource::render_GET(http_request const& req)
    {
        auto const& pieces = req.get_path_pieces();
        
        RequestPath path;
        int skip = _path_skip;
       
        for (string const& p : pieces)
        {
            if (skip > 0)
            {
                --skip;
                continue;
            }

            path.push_back(&p);
        }

        try
        {
            return render_GET(move(path));
        }
        catch (ResourceException& e)
        {
            cout << "ResourceException: " << e.what() << endl;
            return response_error(400);
        }
        catch (exception& e)
        {
            cout << "exception: " << e.what() << endl;
            throw;
        }
    }
}
