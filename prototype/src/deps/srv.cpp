#include "deps/srv.hpp"

using namespace srv;
using namespace std;

namespace srv = httpserver;

namespace iot
{
    http_response response_ok(string const& body)
    {
        return http_response_builder(body, 200, "text/plain")
            .with_header("Access-Control-Allow-Origin", "*")
            .string_response();
    }

    http_response response_error(int code)
    {
        return http_response_builder("", code)
            .with_header("Access-Control-Allow-Origin", "*");
    }
}
