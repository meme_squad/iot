#pragma once

#include "utils/newtype_string.hpp"
#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace iot
{
    void dump(json const&);

    template <typename Tag>
    void to_json(json& j, NewtypeString<Tag> const& s)
    {
        j = json{s.s()};
    }

    template <typename Tag>
    void from_json(json const& j, NewtypeString<Tag>& s) {
        j.get_to(s.s());
    }    
}
