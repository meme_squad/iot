#pragma once

#include "common_types.hpp"
#include "json.hpp"
#include <cpr/cpr.h>

namespace iot
{
    cpr::Response req_get(URL const&);
    cpr::Response req_post(URL const&, json const&);
    cpr::Response req_delete(URL const&);
    cpr::Response req_put(URL const&, json const&);
}
