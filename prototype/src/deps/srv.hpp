#pragma once

#include <httpserver.hpp>
#include <string>

namespace srv = httpserver;

namespace iot
{
    srv::http_response response_ok(std::string const&);
    srv::http_response response_error(int);
}
