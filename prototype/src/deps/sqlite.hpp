#pragma once

#include "common_types.hpp"
#include <sqlite3.h>
#include <stdexcept>

namespace iot
{
    void sqlite_repl(Path const&);

    class SQLException : public std::logic_error
    {
        using std::logic_error::logic_error;
    };
}
