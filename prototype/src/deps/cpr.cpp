#include "cpr.hpp"
#include "utils/trace.hpp"
#include <iostream>

using namespace std;
using namespace cpr;

namespace iot
{
    static
    void log_req(string const& method, URL const& url)
    {
        cerr << "Req " << method << " " << url.s() << "\n";
    }

    Response req_get(URL const& url)
    {
//        TRACE;
        log_req("GET", url);
        return Get(
            Url{url}
            );
    }

    Response req_post(URL const& url, json const& body)
    {
//        TRACE;
        log_req("POST", url);
        return Post(
            Url{url},
            Body{body.dump()},
            Header{{"Content-Type", "text/plain"}}
            );
    }

    Response req_delete(URL const& url)
    {
//        TRACE;
        log_req("DELETE", url);
        return Delete(
            Url{url}
            );
    }

    Response req_put(URL const& url, json const& body)
    {
//        TRACE;
        log_req("PUT", url);
        return Put(
            Url{url},
            Body{body.dump()},
            Header{{"Content-Type", "text/plain"}}
            );
    }
}
