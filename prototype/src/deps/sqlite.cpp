#include "sqlite.hpp"
#include "db.hpp"
#include "utils/signal.hpp"
#include <gsl/gsl>
#include <iostream>

using namespace std;
using namespace gsl;

namespace iot
{
    static 
    void query_loop(DB& db)
    {
        SQL query;

        while (!globals::signal_quit)
        {
            cout << "> ";
            getline(cin, query.s());

            if (query.s().empty() || globals::signal_quit)
            {
                break;
            }

            try
            {
                db.query(query, [](DBRow row)
                {
                    for (auto item : row)
                    {
                        cout << "(" << item.name << " " << item.value << ")";
                    }
                    cout << "\n";
                    return SQLITE_OK;
                });
            }
            catch (SQLException const& ex)
            {
                cerr << "Error: " << ex.what() << endl;
            }
        }
    }
    
    void sqlite_repl(Path const& db_filename)
    {
        DB db{db_filename};
        query_loop(db);
    }
}
