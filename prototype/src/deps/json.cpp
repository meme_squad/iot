#include "json.hpp"
#include <iostream>

using namespace std;

namespace iot
{
    void dump(json const& j)
    {
        cout << j.dump() << "\n";
    }
}
