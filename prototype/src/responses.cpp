#include "responses.hpp"

using namespace std;

namespace iot
{
    json devices_response(vector<APIDevice> const& devices)
    {
        return {"devices", devices};
    }
}
