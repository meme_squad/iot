#pragma once

#include "cap.hpp"
#include "deps/json.hpp"
#include <string>
#include <vector>
#include <optional>

namespace iot
{
    struct APIDeviceID
    {
        std::string major;
        std::string minor;
    };

    struct APIDevice
    {
        APIDeviceID id;
        std::string name;
        std::vector<Cap> caps;
    };

    bool operator==(APIDeviceID const&, APIDeviceID const&);
    bool operator!=(APIDeviceID const&, APIDeviceID const&);
    std::string to_string(APIDeviceID const&);

    void to_json(json&, APIDevice const&);
    void to_json(json&, std::vector<APIDevice> const&);
    std::optional<APIDeviceID> parse_device_id(std::string const&);
}
