#include "cmd.hpp"

using namespace std;

namespace iot
{
    optional<APIDevice> ApplyCmd::operator()(CmdPower const& cmd)
    {
        return map([&](CapPower cap) -> optional<CapPower>
        {
            cap.now = cmd.on;
            return cap;
        });
    }
    
    optional<APIDevice> ApplyCmd::operator()(CmdBri const& cmd)
    {
        return map([&](CapBri cap) -> optional<CapBri>
        {
            if (cmd.bri >= cap.min && cmd.bri <= cap.max)
            {
                cap.now = cmd.bri;
                return cap;
            }
            
            return nullopt;
        });
    }
    
    std::optional<APIDevice> apply_cmd(APIDevice const& dev, Cmd const& cmd)
    {
        return visit(ApplyCmd{dev}, cmd);
    }
}
