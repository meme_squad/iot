#pragma once

#include "deps/srv.hpp"
#include "resource.hpp"

namespace iot::resources
{
    class Command : public Resource
    {
        public:
            srv::http_response render_GET(RequestPath&&);
    };
}
