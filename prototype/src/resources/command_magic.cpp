#include "resources/command_magic.hpp"
#include "utils/unordered_map.hpp"

using namespace std;
using namespace srv;

namespace iot
{
    FromString::FromString(string const& str)
        : _str(str)
    {}

    FromString::operator string const&() const
    {
        return _str;
    }

    FromString::operator bool() const
    {
		static unordered_map<string, bool> const bools
		{
			{"0",     false},
			{"off",   false},
			{"false", false},
			{"1",     true},
			{"on",    true},
			{"true",  true},
		};

		if (auto b = lookup(bools, _str))
		{
			return *b;
		}
		else
		{
            throw ResourceException("bad bool");
        }
    }

	FromString::operator int() const
	{
        try
        {
            return stoi(_str);
        }
        catch (invalid_argument&)
        {
            throw ResourceException("bad int");
        }
        catch (out_of_range&)
        {
            throw ResourceException("bad int");
        }
	}
}
