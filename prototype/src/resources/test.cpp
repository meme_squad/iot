#include "test.hpp"
#include "modules/philips.hpp"
#include "utils/unordered_map.hpp"
#include <optional>
#include <string>

using namespace std;
using namespace std::literals;
using namespace srv;

namespace iot::resources
{
    http_response Test::render_GET(RequestPath&&)
    {
        return response_ok("test unimplemented");
    }
    /*
    enum class Cmd
    {
        on,
        off,
        status,
        brightness,
    };

    static
    unordered_map<string, Cmd> const cmds
    {
        {"on",          Cmd::on},
        {"off",         Cmd::off},
        {"status",      Cmd::status},
        {"bri",         Cmd::brightness},
    };

    http_response Test::render_GET(RequestPath&& path)
    {
        string const& deviceid = "1"; // TODO
        string output;

        auto emit_status = [&output](string const& id)
        {
            output += philips::test_get_status(id).dump();
        };

        if (path.size() == 2)
        {
            if (*path[1] == "init")
            {
                if (philips::test_init())
                {
                    output = "INIT SUCCEEDED";
                }
                else
                {
                    output = "INIT FAILED";
                }
            }
        }
        else if (path.size() >= 3)
        {
            if (auto c = lookup(cmds, *path[2]))
            {
                switch (*c)
                {
                    case Cmd::on: 
                        philips::test_turn_on(deviceid);
                        emit_status(deviceid);
                        break;
                    case Cmd::off: 
                        philips::test_turn_off(deviceid);
                        emit_status(deviceid);
                        break;
                    case Cmd::status: 
                        emit_status(deviceid);
                        break;
                    case Cmd::brightness:
                        if (path.size() == 4)
                        {
                            int bri = stol(*path[3]);
                            philips::test_set_brightness(deviceid, bri);
                            emit_status(deviceid);
                        }
                        break;
                }
            }
            else
            {
                output.clear();
            }
        }

        return response_ok(output);
    }
    */
}
