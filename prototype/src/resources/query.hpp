#pragma once

#include "deps/srv.hpp"
#include "resource.hpp"

namespace iot::resources
{
    class Query : public Resource
    {
        public:
            srv::http_response render_GET(RequestPath&&);
   
        private:
            srv::http_response query_all();
            srv::http_response query(std::string const& dev_id_str);
    };
}
