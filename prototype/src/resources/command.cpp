#include "resources/command.hpp"
#include "resources/command_magic.hpp"
#include "responses.hpp"
#include "cmd.hpp"
#include "utils/color.hpp"

using namespace std;
using namespace srv;

namespace iot::resources
{
    optional<APIDevice> try_cmd(Env& env, APIDeviceID const& dev_id, 
            Cmd const& cmd)
    {
        if (auto module = env.lookup_module(dev_id))
        {
            return module->cmd(dev_id, cmd);
        }
        else
        {
            return nullopt;
        }
    }
    
    http_response cmd_power(Env& env, APIDeviceID const& id, bool on)
    {
        cout << color_on(32) << "cmd_power " << on << "\n" << color_off;
        CmdPower cmd{on};
        
        //TODO encapsulate. ALso get an error back, not just optional.
        if (auto result = try_cmd(env, id, cmd))
        {
            return response_ok(
                    devices_response({*result}).dump()
                    );
        }
        else
        {
            return response_ok("power-error");
        }
    }
    
    http_response cmd_bri(Env& env, APIDeviceID const& id, int bri)
    {
        cout << color_on(32) << "cmd_bri " << bri << " \n" << color_off;
        CmdBri cmd{bri};
        
        if (auto result = try_cmd(env, id, cmd))
        {
            return response_ok(
                    devices_response({*result}).dump()
                    );
        }
        else
        {
            return response_ok("bri-error");
        }
    }
    
    http_response cmd_query(Env& env, APIDeviceID const& id)
    {
        cout << color_on(32) << "cmd_query\n" << color_off;
        //TODO this is duplicated from Query::query.
        if (auto module = env.lookup_module(id))
        {
            if (auto device = module->query(id))
            {
                return response_ok(
                        devices_response({*device}).dump()
                        );
            }
            else
            {
                throw ResourceException("no device with id");
            }
        }
        else
        {
            throw ResourceException("no module with id");
        }
    }
    
    static 
    CommandMap const commands
    {
        command("power", cmd_power),
        command("bri",   cmd_bri),
        command("query", cmd_query),
    };

    http_response Command::render_GET(RequestPath&& path)
    {
        cout << color_on(32) << "Command\n" << color_off;
        if (path.size() < 2)
        {
            throw ResourceException("expected device_id and command");
        }

        if (auto device_id = parse_device_id(*path[0]))
        {
            if (auto cmd_fn = lookup(commands, *path[1]))
            {
                return (*cmd_fn)(*_env, *device_id, path);
            }
            else
            {
                throw ResourceException("unknown command");
            }
        }
        else
        {
            throw ResourceException("bad device_id");
        }
    }
}
