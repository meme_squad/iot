#pragma once

#include "resource.hpp"
#include "device.hpp"
#include "utils/unordered_map.hpp"
#include <string>
#include <utility>
#include <functional>

namespace iot
{
    class FromString
    {
        std::string const& _str;

        public:
            FromString(std::string const&);
            operator std::string const&() const;
            operator bool() const;
            operator int() const;
    };

    template <typename T, size_t... ArgNums>
    srv::http_response call_(
            Env& env,
            APIDeviceID const& dev_id, 
            RequestPath const& path, 
            T const& cmd, 
            std::index_sequence<ArgNums...>
            )
    {
        // First arg is at +2 in the path because of command and dev_id.
        if (path.size() != sizeof...(ArgNums) + 2)
        {
            throw ResourceException("mismatched arity");
        }
        
        return cmd(env, dev_id, FromString(*path[ArgNums + 2])...);
    }

	template <typename... Args>
	using CommandFn = srv::http_response(Env&, APIDeviceID const&, Args...);

    template <typename... Args>
    srv::http_response call(
            Env& env,
            APIDeviceID const& dev_id,
            RequestPath const& path, 
            CommandFn<Args...> const& cmd
            )
    {
        return call_(
                env, 
                dev_id, 
                path, 
                cmd, 
                std::make_index_sequence<sizeof...(Args)>()
                );
    }    

	using CommandStub = std::function<
        srv::http_response(Env&, APIDeviceID const&, RequestPath const&)
        >;

    using CommandPair = std::pair<std::string, CommandStub>;

    template <typename T>
    CommandPair command(std::string const& name, T const& cmd)
    {
        return {
            name,
            [&cmd](Env& env, APIDeviceID const& dev_id, RequestPath const& path)
            {
                return call(env, dev_id, path, cmd);
            }
        };
    }

    using CommandMap = std::unordered_map<std::string, CommandStub>;
}
