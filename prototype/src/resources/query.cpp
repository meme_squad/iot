#include "resources/query.hpp"
#include "device.hpp"
#include "consts.hpp"
#include "responses.hpp"
#include "utils/color.hpp"
#include <optional>
#include <string>
#include <algorithm>
#include <iterator>

using namespace std;
using namespace std::literals;
using namespace srv;

namespace iot::resources
{
    http_response query_all();
    http_response query(string const& dev_id_str);

    http_response Query::render_GET(RequestPath&& path)
    {
        cout << color_on(32) << "Query\n" << color_off;

        switch (path.size())
        {
            case 0:  return query_all();
            case 1:  return query(*path[0]);
            default: throw ResourceException("bad query");
        }
    }

    http_response Query::query_all()
    {
        vector<APIDevice> devices;
            
        auto append = [&](vector<APIDevice>&& ds)
        {
            move(ds.begin(), ds.end(), back_inserter(devices));
        };

        for (auto const& m : _env->modules)
        {
            append(m->query());
        }
        
        for (auto const& d : devices)
        {
            cout << to_string(d.id) << "\n";
        }

        return response_ok(
                devices_response(devices).dump()
                );
    }

    http_response Query::query(string const& dev_id_str)
    {
        cout << "query: " << dev_id_str << "\n";

        if (auto dev_id = parse_device_id(dev_id_str))
        {
            cout << "dev_id: " << to_string(*dev_id) << "\n";
            
            if (auto module = _env->lookup_module(*dev_id))
            {
                if (auto device = module->query(*dev_id))
                {
                    return response_ok(
                            devices_response({*device}).dump()
                            );
                }
                else
                {
                    throw ResourceException("no device with id");
                }
            }
            else
            {
                throw ResourceException("no module with id");
            }
        }
        else
        {
            throw ResourceException("failed to parse device id");
        }
    }
}
