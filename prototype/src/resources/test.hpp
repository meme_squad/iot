#pragma once

#include "resource.hpp"

namespace iot::resources
{
    class Test : public Resource
    {
        public:
            srv::http_response render_GET(RequestPath&&);
    };
}
