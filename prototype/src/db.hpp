#pragma once

#include "common_types.hpp"
#include "deps/sqlite.hpp"
#include <memory>
#include <functional>

namespace iot 
{
    struct DBRowData
    {
        int    size;
        char** values;
        char** names;
    };

    class DBRow
    {
        public:
            class Iter
            {
                public:
                    Iter(DBRowData, int col);
                    Iter& operator++();
                    bool operator==(Iter const&) const;
                    bool operator!=(Iter const&) const;
                
                    struct Pair
                    {
                        char const* name;
                        char const* value;
                    };

                    Pair operator*();

            private:
                DBRowData _row;
                int       _col;
            
                bool at_end() const;
        };

        public:
            DBRow(DBRowData);
            Iter begin() const;
            Iter end() const;
        
        private:
            DBRowData _row;
    };
    
    using SQLCallback = int(void*,int,char**,char**);

    class DB
    {
        public:
            DB(Path const&);

            template <typename F>
            int query(SQL const& sql, F const& func)
            {
                auto callback = 
                    [](void* data, int size, char** values, char** names)
                {
                    F const* func = static_cast<F const*>(data);
                    return (*func)(DBRow{{size, values, names}});
                };

                // I swear on my life not to mutate func.
                // SQLite3 just requires a void*.
                return query(sql, callback, &const_cast<F&>(func));
            }

            int query(SQL const&);

        private:
            void open(Path const&);
            int query(SQL const&, SQLCallback*, void* callback_data);
            
            struct Deleter
            {
                void operator()(sqlite3* p) const
                {
                    sqlite3_close(p);
                }
            };

            std::unique_ptr<sqlite3, Deleter> _db;
    };
}
