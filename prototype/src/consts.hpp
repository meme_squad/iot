#pragma once

#include "common_types.hpp"
#include <vector>
#include <string>

namespace iot::consts
{
    extern int  const default_port;
    extern Path const config_filename;
    extern char const device_id_separator;
    extern std::vector<std::string> const api_root;
}
