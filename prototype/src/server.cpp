#include "server.hpp"

using namespace srv;
using namespace std;

namespace iot 
{
    Server::Server(Env* env, int port)
        : _env(env)
        , _server(create_webserver(port)
            .start_method(http::http_utils::INTERNAL_SELECT)
            .max_threads(5)
            .debug())
    {}

    Server::~Server()
    {
        _server.stop();
    }

    void Server::start()
    {
        _server.start(false);
    }

    void Server::add(unique_ptr<Resource>&& res, vector<string>&& path_pieces)
    {
        std::string path;
        std::string const slash{ "/" };

        for (auto const& p : consts::api_root)
        {
            path.append(slash + p);
        }
        
        for (auto const& p : path_pieces)
        {
            path.append(slash + p);
        }
        
        int const path_skip = consts::api_root.size() 
                            + path_pieces.size();

        res->init(_env, path_skip);
        http_resource* srv_res = res.get();
        _storage.push_back(move(res));
        _server.register_resource(path, srv_res, true);
        cout << path << " " << path_skip << "\n";
    }
}

