#pragma once

#include "deps/srv.hpp"
#include "env.hpp"
#include <vector>
#include <string>

namespace iot
{
    using RequestPath = std::vector<std::string const*>;
    
    class ResourceException : public std::runtime_error
    {
        using std::runtime_error::runtime_error;
    };
    
    class Resource : public srv::http_resource
    {
        public:
            void init(Env* env, int path_skip)
            {
                _env = env;
                _path_skip = path_skip;
            }

            virtual srv::http_response render_GET(RequestPath&& path) = 0;
            srv::http_response const render_GET(srv::http_request const&);
        
        protected: 
            Env* _env = nullptr;

        private:
            int _path_skip = 0;

    };
}
