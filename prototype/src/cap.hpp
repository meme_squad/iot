#pragma once

#include "deps/json.hpp"
#include <variant>
#include <vector>

namespace iot
{
    struct CapPower
    {
        bool now;
    };

    void to_json(json&, CapPower const&);
    void from_json(json const&, CapPower&); 
    
    struct CapBri
    {
        int min;
        int max;
        int now;
    };
    
    void to_json(json&, CapBri const&);
    void from_json(json const&, CapBri&); 
    
    using Cap = std::variant<CapPower,CapBri>;
    
    void to_json(json&, std::vector<Cap> const&);
    
    struct CapToJson
    {
        json& j;

        void operator()(CapPower const&);
        void operator()(CapBri const&);
    };
}
