#pragma once

#include "module.hpp"
#include <vector>
#include <memory>

namespace iot
{
    struct Env
    {
        template <typename... Ts>
        void make_modules()
        {
            (modules.push_back(std::make_unique<Ts>()), ...);
        }

        Module* lookup_module(APIDeviceID const&);
        Module const* lookup_module(APIDeviceID const&) const;
        void init_modules();

        std::vector<std::unique_ptr<Module>> modules;
        bool init = false;
    };
}
