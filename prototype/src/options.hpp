#pragma once

#include "consts.hpp"
#include "common_types.hpp"
#include <optional>
#include <string>

namespace iot
{
    struct Options
    {
        int port;
        bool mock;
        std::optional<Path> repl;

        Options();
    };

    Options get_options(int argc, char** argv);
}
