#include "consts.hpp"

using namespace std;

namespace iot::consts
{
    int  const default_port{ 8000 };
    Path const config_filename{ "srp.cfg" };
    char const device_id_separator{ '-' };
    vector<string> const api_root{ "api" };
}
