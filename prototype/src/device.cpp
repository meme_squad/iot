#include "device.hpp"
#include "utils/unordered_map.hpp"
#include "consts.hpp"
#include <iostream>

using namespace std;

namespace iot
{
    bool operator==(APIDeviceID const& a, APIDeviceID const& b)
    {
        return a.major == b.major
            && a.minor == b.minor;
    }

    bool operator!=(APIDeviceID const& a, APIDeviceID const& b)
    {
        return !(a == b);
    }

    string to_string(APIDeviceID const& x)
    {
        return x.major
             + consts::device_id_separator
             + x.minor;
    }
    
    void to_json(json& j, APIDevice const& x)
    {
        j = json {
            {"name", x.name},
            {"caps", x.caps},
        };
    }
    
    void to_json(json& j, vector<APIDevice> const& x)
    {
        for (auto const& dev : x) {
            j[to_string(dev.id)] = dev;
        }
    }

    optional<APIDeviceID> parse_device_id(string const& device_str)
    {
        APIDeviceID device_id;
        
        auto separator = find(
                device_str.begin(),
                device_str.end(),
                consts::device_id_separator
                );

        if (separator == device_str.end())
        {
            return nullopt;
        }

        copy(device_str.begin(), separator, back_inserter(device_id.major));
        copy(++separator, device_str.end(), back_inserter(device_id.minor));

        if (device_id.major.empty() || device_id.minor.empty())
        {
            return nullopt;
        }
        
        return device_id;
    }
}
