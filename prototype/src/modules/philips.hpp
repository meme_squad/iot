#pragma once

#include "module.hpp"
#include "utils/any.hpp"
#include "utils/newtype_string.hpp"
#include "utils/optional.hpp"
#include "utils/string.hpp"
#include "utils/unordered_map.hpp"
#include "utils/vector.hpp"

namespace iot::modules
{
            
    NEW_STRING_TYPE_(UserName);
    class Philips : public Module
    {
        public:
            Philips();
            string module_name() const;
            void load_config();
            void init_database();
            vector<APIDevice> query() const;
            optional<APIDevice> query(APIDeviceID const&) const;
            optional<APIDevice> cmd(APIDeviceID const&, Cmd const&);

        private:

            struct Hub
            {
                string id;
                string address;
            };
            
            struct APIError
            {
                int type;
                string address;
                string description;
            };

            struct APIState
            {
                string id;
                bool state_on;
                int state_bri;
            };

            struct Device
            {
                string unique_id;
                string model_id;
                string path;
                string name;
                Hub    hub;
                unordered_map<string, any> state_map;

                template <typename T>
                optional<T> state(string const& key) const
                {
                    if (any const* any_value = lookup(state_map, key))
                    {
                        if (T const* t_value = any_cast<T>(&*any_value))
                        {
                            return *t_value;
                        }
                    }

                    return nullopt;
                }
            }; 
            
            struct Status
            {
                Hub            hub;
                vector<Device> devices;
            };
           
//            string url_root();
//            bool check(json const& j);
//            json first(json&& j);
//            optional<UserName> become_dev();
//            optional<APIState> get_state(UserName const&, string const& id);
//            bool put_state(UserName const&, string const& id, string const& str);
//            bool turn_on(UserName const&, string const& id);
//            bool turn_off(UserName const&, string const& id);
//            bool set_brightness(UserName const&, string const& id, int bri);
//            bool delete_user(UserName const& username, UserName const& username_del);
//            optional<UserName> auth();


            void print_device(Device const&) const;
            void print_device(APIDevice const&) const;
            optional<Device> respecialize(APIDevice const&, Device) const;
            optional<APIDevice> generalize(Device const&) const;
            optional<Device> query_real(APIDeviceID const&) const;
            optional<Status> query_status(Hub const&, UserName const&) const;
            optional<UserName> become_dev(Hub const&) const;
            bool write_device(Device const&);
            vector<Hub> hubs() const;
            vector<Device> devices(Hub const&) const;
            void write_username(Hub const&, UserName const&) const;
            optional<UserName> read_username(Hub const&) const;
    
            mutable vector<Hub> _hubs; //TODO hack
            mutable unordered_map<string, Device> _devices; //TODO also hack
    };
}
