#include "modules/dummy.hpp"
#include "cap.hpp"
#include "consts.hpp"
#include "db.hpp"
#include "deps/sqlite.hpp"
#include "deps/srv.hpp"
#include <functional>

using namespace std;

namespace iot::modules
{
    using std::to_string;
    
    Dummy::Dummy()
    {}

    string Dummy::module_name() const
    {
        return "dummy";
    }
    
    void Dummy::load_config()
    {
        //TODO
    }

    void Dummy::init_database()
    {
        DB db = open_db();
        
        db.query("CREATE TABLE IF NOT EXISTS devices (device_id TEXT PRIMARY KEY, power INTEGER, bri INTEGER)"_sql);

        APIDevice dev_a{
            make_id("light1"),
            "name1",
            { CapPower{ false }, CapBri{ 0, 254, 45 } }
        };

        APIDevice dev_b{
            make_id("light2"),
            "name2",
            { CapPower{ true }, CapBri{ 0, 254, 128 } }
        };

        write_device(dev_a);
        write_device(dev_b);
    }

    vector<APIDevice> Dummy::query() const
    {
        vector<APIDevice> result;
        
        DB db = open_db();
        SQL const query{ "SELECT * FROM devices" };
        
        db.query(query, [&](DBRow row)
        {
            APIDevice device;
            CapPower  cap_power;
            CapBri    cap_bri;

            cap_power.now = false;
            cap_bri.min = 0;
            cap_bri.max = 254;
            cap_bri.now = 0;
            device.id = make_id("");
            
            for (auto [name, value] : row)
            {
                //TODO more typesafe sqlite3 results
                if (strcmp(name, "device_id") == 0)
                {
                    device.id.minor = value;
                }
                else if (strcmp(name, "power") == 0)
                {
                    cap_power.now = (strcmp(value, "0") != 0);
                }
                else if (strcmp(name, "bri") == 0)
                {
                    cap_bri.now = stol(value);
                }
            }
            
            device.caps = { cap_power, cap_bri };
            result.push_back(std::move(device));
            return SQLITE_OK;
        });
        
        cout << "dummy: ";
        for (auto const& r : result)
        {
            cout << to_string(r.id) << " ";
        }
        cout << "\n";
        return result;
    }

    optional<APIDevice> Dummy::query(APIDeviceID const& dev_id) const
    {
        for (auto const& dev : query())
        {
            if (dev.id == dev_id)
            {
                return dev;
            }
        }

        cout << "dummy: no device " << to_string(dev_id) << "\n";
        return nullopt;
    }

    optional<APIDevice> Dummy::cmd(APIDeviceID const& dev_id, Cmd const& cmd)
    {
        for (auto const& dev0 : query())
        {
            if (dev0.id == dev_id)
            {
                if (auto dev1 = apply_cmd(dev0, cmd))
                {
                    write_device(*dev1);
                    return dev1;
                }
                else
                {
                    cout << "dummy: " << to_string(dev_id) << " has not cap\n";
                    return nullopt;
                }
            }
        }

        cout << "dummy: no device " << to_string(dev_id) << "\n";
        return nullopt;
    }
        
    void Dummy::write_device(APIDevice const& dev)
    {
        string dev_id = dev.id.minor;
        optional<string> bri;
        optional<string> power;

        for (auto const& cap : dev.caps)
        {
            if (auto c = get_if<CapPower>(&cap))
            {
                power = to_string(c->now);
            }
            else if (auto c = get_if<CapBri>(&cap))
            {
                bri = to_string(c->now);
            }
        }

        if (!bri || !power)
        {
            throw logic_error("dummy: dev didn't have expected caps");
        }
        
        DB db = open_db();
        SQL query{
            "INSERT OR REPLACE INTO devices(device_id,power,bri) VALUES('"
            + dev_id
            + "',"
            + *power
            + ","
            + *bri
            + ")"
        };
        db.query(query);
    }
}
