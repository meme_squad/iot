#include "philips.hpp"
#include "utils/trace.hpp"
#include "utils/file.hpp"
#include "utils/thread.hpp"
#include "deps/cpr.hpp"
#include "utils/unordered_set.hpp"
#include "utils/color.hpp"

using namespace std;

/*
	"1": {
		"state": {
			"on": true,
			"bri": 254,
		},
		"modelid": "LWB014",
		"uniqueid": "00:17:88:01:02:1c:59:9d-0b",
	},
	"2": {
		"state": {
			"on": true,
			"bri": 1,
		},
		"modelid": "LWB014",
		"uniqueid": "00:17:88:01:02:1c:59:a9-0b",
    }
*/
namespace iot::modules
{
    using std::to_string;

    void Philips::print_device(Device const& d) const
    {
        cout << d.unique_id << "\n";
//        cout << d.model_id  << "\n";
//        cout << d.hub.address << "\n";
        cout << d.path      << "\n";
//        cout << d.name      << "\n";
       
        for (auto [k, v] : d.state_map)
        {
            if (auto* x = any_cast<int>(&v))
            {
                cout << k << " int " << *x << "\n";
            }
            else if (auto* x = any_cast<bool>(&v))
            {
                cout << k << " bool " << *x << "\n";
            }
            else
            {
                cout << k << " ?\n";
            }
        }
    }

    struct CapPrinter
    {
        void operator()(CapPower const& x)
        {
            cout << "CapPower{" << x.now << "}\n";
        }
        
        void operator()(CapBri const& x)
        {
            cout << "CapBri{" << x.min << "," << x.max << "," << x.now << "}\n";
        }
    };
    
    void Philips::print_device(APIDevice const& d) const
    {
//        cout << d.id.major << "\n";
        cout << d.id.minor  << "\n";

        CapPrinter p;
       
        for (auto&& c : d.caps)
        {
            visit(p, c);
        }
    }

    bool check(json const& j)
    {
        if (j.count("error"))
        {
            json const& je = j["error"];
            int type         = je["type"];
            string address      = je["address"];
            string description  = je["description"];
        
            cout
                << "APIError{"
                << "type:" << type << ","
                << "address:\"" << address << "\","
                << "description:\"" << description << "\"}\n";

            return false;
        }
        else
        {
//            cout << "SUCCESS: ";
//            dump(j);
        }

        return true;
    }

    json first(json&& j)
    {
        if (j.type() == json::value_t::array)
        {
            return j[0];
        }
        else
        {
            return j;
        }
    }

    Philips::Philips()
    {}

    auto Philips::module_name() const
        -> string
    {
        return "philips";
    }
   
    // TODO weak
    auto verify_addr(string const& s)
        -> bool
    {
        if (s.empty())
        {
            return false;
        }

        for (auto c : s)
        {
            if (c == '.' || (c >= '0' && c <= '9'))
            {
                continue;
            }

            return false;
        }

        return true;
    }
        
    void Philips::load_config()
    {
        string const path = cfg_filename().s();

        //TODO proper cfg options
        for (auto const& line : lines_in(path))
        {
            if (verify_addr(line))
            {
                //TODO hub id, verify, etc
                Hub hub { line, line };
                _hubs.push_back(hub);
                cout << "Philips hub: " << line << "\n";

                if (!read_username(hub))
                {
                    become_dev(hub);
                }
            }
        }
    }
    
    void Philips::init_database()
    {
        DB db = open_db();
        db.query("CREATE TABLE IF NOT EXISTS users (hub TEXT PRIMARY KEY, username TEXT)"_sql);
    }

    auto Philips::query() const
        -> vector<APIDevice>
    {
//        TRACE;
        vector<APIDevice> result;
        _devices.clear(); //TODO hacky

        for (Hub const& hub : hubs())
        {
            for (Device const& dev : devices(hub))
            {
                if (auto api_dev = generalize(dev))
                {
                    _devices[api_dev->id.minor] = dev;
                    result.push_back(*api_dev);
                }
            }
        }

        return result;
    }
    
    auto Philips::query(APIDeviceID const& dev_id) const
        -> optional<APIDevice>
    {
        //TODO add a cache
        for (APIDevice const& dev : query())
        {
            if (dev.id == dev_id)
            {
                return dev;
            }
        }

        return nullopt;
    }
     
    auto Philips::cmd(APIDeviceID const& dev_id, Cmd const& cmd)
        -> optional<APIDevice>
    {
        query(); //TODO BAD
        string const& id = to_string(dev_id);

        if (optional<Device> real_dev0 = query_real(dev_id))
        {
            if (optional<APIDevice> api_dev0 = generalize(*real_dev0))
            {
                if (optional<APIDevice> api_dev1 = apply_cmd(*api_dev0, cmd))
                {
                    if (optional<Device> real_dev1 = respecialize(
                                                        *api_dev1,
                                                        *real_dev0))
                    {
                        cout << color_on(35);
                        print_device(*real_dev0);
                        print_device(*api_dev0);
                        cout << "---------\n";
                        print_device(*real_dev1);
                        print_device(*api_dev1);
                        cout << color_off;
                        
                        if (write_device(*real_dev1))
                        {
                            return api_dev1;
                        }
                        else
                        {
                            cout << "cmd: failed to write " << id << "\n";
                            return nullopt;
                        }
                    }
                    else
                    {
                        cout << "cmd: failed to respecialize " << id << "\n";
                        return nullopt;
                    }
                }
                else
                {
                    cout << "cmd: failed to apply cmd on " << id << "\n";
                    return nullopt;
                }
            }
            else
            {
                cout << "cmd: failed to generalize " << id << "\n";
                return nullopt;
            }
        }
        else
        {
            cout << "cmd: failed to query real " << id << "\n";
            return nullopt;
        }
    }
   
    struct CapToState
    {
        using Result = optional<pair<string, any>>;

        Result operator()(CapPower const& x)
        {
            return pair{ "on", x.now };
        }
        
        Result operator()(CapBri const& x)
        {
            return pair{ "bri", x.now };
        }
    };

    auto Philips::respecialize(APIDevice const& api_dev, Device real_dev) const
        -> optional<Device>
    {
        CapToState cap_to_state{};

        for (auto const& cap : api_dev.caps)
        {
            if (auto state = visit(cap_to_state, cap))
            {
                real_dev.state_map[state->first] = state->second;
            }
            else
            {
                cout << "failed to respecialize a cap\n";
                return nullopt;
            }
        }

        return real_dev;
    }

    auto Philips::generalize(Device const& dev) const
        -> optional<APIDevice>
    {
        APIDevice api_dev{ 
            make_id(dev.unique_id), 
            dev.name,
            vector<Cap>{} 
        };

        if (dev.model_id == "LWB014") //TODO hack
        {             
            optional<bool> power_now = dev.state<bool>("on");
            optional<int>  bri_now   = dev.state<int>("bri");

            if (!power_now || !bri_now)
            {
                cout << "failed to generalize (status)\n";
                return nullopt;
            }

            CapPower power;
            power.now = *power_now;

            CapBri bri;
            bri.min = 0;
            bri.max = 255;
            bri.now = *bri_now;
            
            api_dev.caps = { power, bri };
        }
        else if (dev.model_id == "LCT011") //TODO ALSO HACK
        {
            optional<bool> power_now = dev.state<bool>("on");
            optional<int>  bri_now   = dev.state<int>("bri");

            if (!power_now || !bri_now)
            {
                cout << "failed to generalize (status)\n";
                return nullopt;
            }

            CapPower power;
            power.now = *power_now;

            CapBri bri;
            bri.min = 0;
            bri.max = 255;
            bri.now = *bri_now;
            
            api_dev.caps = { power, bri };
        }
        else
        {
            cout << "failed to generalize (model)\n";
            return nullopt;
        }
        
        return api_dev;
    }

    auto Philips::query_real(APIDeviceID const& dev_id) const
        -> optional<Device>
    {
        //TODO faked
//        cout << "query_real devices " << _devices.size() << ":\n";
//        for (auto [k,v] : _devices)
//        {
//            cout << k << "\n";
//        }

        if (auto device = lookup(_devices, dev_id.minor))
        {
            return *device;
        }
        else
        {
            return nullopt;
        }
    }

    bool Philips::write_device(Device const& dev)
    {
        if (auto username = read_username(dev.hub))
        {
            URL const url{ dev.hub.address
                         + "/api/" 
                         + *username
                         + "/"
                         + dev.path 
                         + "/state"
                         };
            json j;
           
            //TODO total hack, do it logically later
            bool power = false;

            for (auto [k, v] : dev.state_map)
            {
                if (k == "on")
                {
                    power = any_cast<bool>(v); // TODO CRASH ZONE
                    break;
                }
            }
           
            static unordered_set<string> const on_restricted { 
                "bri"
            };

            for (auto [k, v] : dev.state_map)
            {
                if (!power && lookup(on_restricted, k))
                {
                    continue;
                }

                if (auto* x = any_cast<int>(&v))
                {
                    j[k] = *x;
                }
                else if (auto* x = any_cast<bool>(&v))
                {
                    j[k] = *x;
                }
                else
                {
                    cout << "write_device failed on " << k << "\n";
                }
            }

            cout << color_on(33);
            cpr::Response const rsp = req_put(url, j);
            json const rsp_j = first(rsp.text);
            cout << j.dump() << "\n";
            cout << rsp.text << "\n";
            cout << color_off;

            if (!check(rsp_j))
            {
                cout << "write_device failed req\n";
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    
    auto Philips::hubs() const
        -> vector<Hub>
    {
        //TODO discovery
        return _hubs;
    }

    auto Philips::query_status(Hub const& hub, UserName const& username) const
        -> optional<Status>
    {
        Status result;
        result.hub = hub;

        URL const url{ hub.address
                     + "/api/" 
                     + username 
                     };
        cpr::Response const rsp = req_get(url);
        json const j = first(json::parse(rsp.text));

        if (!check(j))
        {
            cout << "query_status failed req\n";
            return nullopt;
        }

        json const lights = j["lights"];

        for (auto i = lights.begin(); i != lights.end(); ++i)
        {
            json const l = *i;
            json const s = l["state"]; 
           // cout << color_on(31) << l.dump() << color_off << "\n";
            
            Device dev;
            dev.unique_id = l["uniqueid"];
            dev.model_id  = l["modelid"];
            dev.hub       = hub;
            dev.path      = "lights/" + i.key();
            dev.name      = l["name"];
//            //TODO hardcoded state members! will vary
            dev.state_map["on"]  = bool{ s["on"] }; 
            dev.state_map["bri"] = int{  s["bri"] };
           
            result.devices.push_back(move(dev));
        }

        return result;
    }

    auto Philips::devices(Hub const& hub) const 
        -> vector<Device>
    {
        if (auto username = read_username(hub))
        {
            if (auto status = query_status(hub, *username))
            {
                return status->devices;
            }
            else
            {
                cout << "failed to request status on " << hub.address << "\n";
                return {};
            }
        }
        else
        {
            cout << "failed to query username on " << hub.address << "\n";
            return {};
        }
    }
    
    void Philips::write_username(Hub const& hub, UserName const& username) const
    {
//        TRACE;
        DB db = open_db();
        SQL const query{
            "INSERT INTO users (hub, username) VALUES('" 
                + hub.id
                + "','" 
                + username
                + "')"
            };
        db.query(query);
    }
    
    auto Philips::read_username(Hub const& hub) const
        -> optional<UserName>
    {
//        TRACE;
        optional<UserName> result;
        DB db = open_db();
        SQL const query{
            "SELECT username FROM users WHERE hub='" 
                + hub.id
                + "'" 
            };
        
        db.query(query, [&result](DBRow row)
        {
            for (auto item : row)
            {
                if (strcmp(item.name, "username") == 0)
                {
                    result = UserName{item.value};
                    return SQLITE_ABORT;
                }
            }
            
            return SQLITE_OK;
        });
        
        if (result)
        {
            cout << "read_username: " << result->s() << "\n";
        }
        else
        {
            cout << "read_username: <failed>\n";
        }

        return result;
    }

    optional<UserName> Philips::become_dev(Hub const& hub) const
    {
//        TRACE;
        json const msg = R"(
        {
            "devicetype":"meme_team"
        })"_json;
       
        URL const url{ hub.address + "/api" };// TODO hardcoded

        while (true)
        {
            cout << "Press hub button..\n";
            iot::sleep(2000); //TODO easy clsah with ::sleep
            cpr::Response const rsp = req_post(url, msg);
            cout << rsp.text << "\n";
            json const j = first(json::parse(rsp.text));
        
            if (check(j))
            {
                auto username = j["success"]["username"];
                write_username(hub, username);
                return username;
            }
        }

        return {};
    }

//    optional<APIState> get_state(UserName const& username, string const& id)
//    {
//        TRACE;
//        URL const url{
//            url_root() 
//          + "/" 
//          + username
//          + "/lights/" 
//          + id
//            };
//        cpr::Response const r = req_get(url);
//        json const j = first(json::parse(r.text));
//        
//        if (check(j))
//        {
//            json const k = j["state"];
//            APIState s;
//            s.id        = "light" + id; //TODO
//            s.state_on  = k["on"];
//            s.state_bri = k["bri"];
//            return s;
//        }
//        else
//        {
//            return {};
//        }
//    }
//
//    bool put_state(UserName const& username, string const& id, string const& str)
//    {
//        TRACE;
//        URL const url{
//            url_root() 
//          + "/" 
//          + username
//          + "/lights/" 
//          + id 
//          + "/state"
//            };
//        json const msg = json::parse(str);
//        cpr::Response const r = req_put(url, msg);
//        json const j = first(r.text);
//        return check(j);
//    }
//    
//    bool turn_on(UserName const& username, string const& id)
//    {
//        return put_state(
//                username, 
//                id,
//                "{\"on\":true}"
//                );
//    }
//
//    bool turn_off(UserName const& username, string const& id)
//    {
//        return put_state(
//                username,
//                id,
//                "{\"on\":false}"
//                );
//    }
//
//    bool set_brightness(UserName const& username, string const& id, int i)
//    {
//        return put_state(
//                username,
//                id,
//                "{\"bri\":" + std::to_string(i) + "}"
//                );
//    }
//
//    bool delete_user(UserName const& username, UserName const& username_del)
//    {
//        URL const url{
//            url_root()
//          + "/" 
//          + username
//          + "/config/whitelist/" 
//          + username_del
//            };
//        cpr::Response const r = req_delete(url);
//        json const j = first(r.text);
//        return check(j);
//    }
//
//    optional<UserName> auth()
//    {
//        TRACE;
//        return read_username("philips"_dname);
//    }
}
