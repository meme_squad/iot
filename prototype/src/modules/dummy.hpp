#pragma once

#include "module.hpp"

namespace iot::modules
{
    class Dummy : public Module
    {
        public:
            Dummy();
            std::string module_name() const;
            void load_config();
            void init_database();
            std::vector<APIDevice> query() const;
            std::optional<APIDevice> query(APIDeviceID const&) const;
            std::optional<APIDevice> cmd(APIDeviceID const&, Cmd const&);

        private:
            void write_device(APIDevice const&);
    };
}
