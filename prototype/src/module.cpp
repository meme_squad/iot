#include "module.hpp"

using namespace std;

namespace iot
{
    APIDeviceID Module::make_id(string const& minor) const
    {
        return APIDeviceID{ module_name(), minor };
    }
    
    DB Module::open_db() const
    {
        return DB{ db_filename() };
    }
            
    Path Module::db_filename() const
    {
        return Path { module_name() + ".sqlite" };
    }

    Path Module::cfg_filename() const
    {
        return Path { module_name() + ".cfg" };
    }
}
