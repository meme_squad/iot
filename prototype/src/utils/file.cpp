#include "file.hpp"

using namespace std;

FileLines::FileLines(string const& path)
 : _file(path)
{}

FileLines::Iter FileLines::begin()
{
    return Iter{ &_file };
}

FileLines::Iter FileLines::end() const
{
    return Iter{};
}

FileLines::Iter::Iter(ifstream* file)
    : _file(file)
{}

FileLines::Iter& FileLines::Iter::operator++()
{
    if (!at_end())
    {
        getline(*_file, _line);
    }

    return *this;
}

bool FileLines::Iter::operator==(Iter const& o) const
{
    return (at_end() == o.at_end())
        || (_file && o._file && _file->tellg() == o._file->tellg());
}

bool FileLines::Iter::operator!=(Iter const& o) const
{
    return !(*this == o);
}

string const& FileLines::Iter::operator*() const
{
    return _line;
}

bool FileLines::Iter::at_end() const
{
    return !_file
        || !(*_file);
}

optional<string> read_file(string const& path)
{
    ifstream file(path);

    if (!file)
    {
        return nullopt;
    }

    string s;
    file.seekg(0, ios::end);
    s.reserve(file.tellg());
    file.seekg(0, ios::beg);

    s.assign((istreambuf_iterator<char>(file)),
              istreambuf_iterator<char>());

    return s;
}
