#pragma once

#include <fstream>
#include <string>
#include <optional>

class FileLines
{
    public:
        class Iter
        {
            public:
                Iter() = default;
                Iter(std::ifstream*);
                Iter& operator++();
                bool operator==(Iter const&) const;
                bool operator!=(Iter const&) const;
                std::string const& operator*() const;

        private:
            bool at_end() const;
            
            std::ifstream* _file = nullptr;
            std::string    _line;
        };

    public:
        FileLines(std::string const&);
        Iter begin();
        Iter end() const;
    
    private:
        std::ifstream _file;
};

inline
FileLines lines_in(std::string const& path)
{
    return FileLines(path);
}

std::optional<std::string> read_file(std::string const& path);
