#include "signal.hpp"
#include <signal.h>

namespace iot 
{
    namespace globals
    {
        bool signal_quit = false;
    }

    static void quit_nicely(int)
    {
        globals::signal_quit = true;
    }

    void register_signals()
    {
        signal(SIGTERM, quit_nicely);
        signal(SIGQUIT, quit_nicely);
        signal(SIGINT,  quit_nicely);
        signal(SIGHUP,  quit_nicely);
    }
}
