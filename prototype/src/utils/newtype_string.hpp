#pragma once

#include <string>

#define NEW_STRING_TYPE(type, lit)\
    using type = NewtypeString<struct type##_tag>;\
    inline type operator""_##lit(char const* c, size_t) { return type{c}; }\

#define NEW_STRING_TYPE_(type)\
    using type = NewtypeString<struct type##_tag>

namespace iot
{
    template <typename Tag>
    class NewtypeString
    {
        public:
            NewtypeString() = default;
            explicit NewtypeString(std::string const& s)
                : _s{s}
            {}
            explicit NewtypeString(std::string&& s)
                : _s{std::move(s)}
            {}
            
            char const*        c() const        { return _s.c_str(); }
            std::string&       s()              { return _s; }
            std::string const& s() const        { return _s; }
            
            operator std::string&      ()       { return _s; }
            operator std::string const&() const { return _s; }
            operator std::string&&     () &&    { return std::move(_s); }
        private:
            std::string _s;
    };

    template <typename Tag>
    std::string operator+(std::string const& l, NewtypeString<Tag> const& r)
    {
        return l + r.s();
    }

    template <typename Tag>
    std::string operator+(NewtypeString<Tag> const& l, std::string const& r)
    {
        return l.s() + r;
    }
}
