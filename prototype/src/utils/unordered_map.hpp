#pragma once

#include <unordered_map>

namespace iot
{
    using std::unordered_map;

    template <typename K, typename V>
    V const* lookup(unordered_map<K,V> const& map, K const& key)
    {
        auto it = map.find(key);

        if (it == map.end())
        {
            return {};
        }

        return &it->second;
    }
    
    template <typename K, typename V>
    V* lookup(unordered_map<K,V>& map, K const& key)
    {
        auto it = map.find(key);

        if (it == map.end())
        {
            return {};
        }

        return &it->second;
    }
}
