#pragma once

#include <iostream>

namespace iot
{
    struct ColorOn { int color; };

    inline
    ColorOn color_on(int color)
    {
        return { color };
    }

    struct ColorOff {};
    ColorOff const color_off;

    std::ostream& operator<<(std::ostream&, ColorOn);
    std::ostream& operator<<(std::ostream&, ColorOff);
}

