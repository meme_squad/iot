#pragma once

#include <unordered_set>

namespace iot
{
    using std::unordered_set;

    template <typename T>
    bool lookup(unordered_set<T> const& set, T const& key)
    {
        auto it = set.find(key);
        return it != set.end();
    }
}
