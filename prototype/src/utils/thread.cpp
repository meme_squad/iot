#include "thread.hpp"
#include <chrono>
#include <thread>

using namespace std;
using namespace std::chrono;

namespace iot
{
    void sleep(int ms)
    {
        this_thread::sleep_for(milliseconds{ms});
    }
}
