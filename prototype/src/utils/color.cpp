#include "utils/color.hpp"

using namespace std;

namespace iot
{
    ostream& operator<<(ostream& s, ColorOn c)
    {
        return s << "\033[" << c.color << "m";
    }
    
    ostream& operator<<(ostream& s, ColorOff)
    {
        return s << "\033[0m";
    }
}

