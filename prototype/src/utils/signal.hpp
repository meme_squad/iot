#pragma once

namespace iot 
{
    namespace globals 
    {
        extern bool signal_quit;
    }

    void register_signals();
}
