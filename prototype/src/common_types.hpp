// Contains simple types that are used in many places.

#pragma once

#include "utils/newtype_string.hpp"

namespace iot
{
    NEW_STRING_TYPE(URL,  url)
    NEW_STRING_TYPE(Path, path)
    NEW_STRING_TYPE(SQL,  sql)
}
