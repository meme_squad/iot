#pragma once

#include "cmd_detail.hpp"
#include <variant>

namespace iot
{
    struct CmdPower
    {
        bool on;
    };

    struct CmdBri
    {
        int bri;
    };

    using Cmd = std::variant<
        CmdPower,
        CmdBri
        >;
   
    // Visitor to map a Cmd over an APIDevice 
    struct ApplyCmd
    {
        std::optional<APIDevice> operator()(CmdPower const&);
        std::optional<APIDevice> operator()(CmdBri const&);
       
        APIDevice dev;

        template <typename F>
        std::optional<APIDevice> map(F const& f)
        {
            return detail::map(dev, std::function(f));
        }
    };

    std::optional<APIDevice> apply_cmd(APIDevice const&, Cmd const&);
}
