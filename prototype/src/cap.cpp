#include "cap.hpp"

using namespace std;

namespace iot
{
    void CapToJson::operator()(CapPower const& x)
    {
        j["power"] = x;
    }

    void CapToJson::operator()(CapBri const& x)
    {
        j["bri"] = x;
    }
    
    void to_json(json& j, const CapPower& x) 
    {
        j = json {
            {"now", x.now}
        };
    }

    void from_json(const json& j, CapPower& x) 
    {
        j.at("now").get_to(x.now);
    }

    void to_json(json& j, const CapBri& x) 
    {
        j = json {
            {"min", x.min}, 
            {"max", x.max}, 
            {"now", x.now}
        };
    }

    void from_json(const json& j, CapBri& x) 
    {
        j.at("min").get_to(x.min);
        j.at("max").get_to(x.max);
        j.at("now").get_to(x.now);
    }
    
    void to_json(json& j, vector<Cap> const& x)
    {
        auto v = CapToJson{j};

        for (auto const& cap : x) {
            visit(v, cap);
        }
    }
}
