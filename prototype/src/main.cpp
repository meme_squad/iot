#include "options.hpp"
#include "server.hpp"
#include "test.hpp" // TESTING
#include "env.hpp"
#include "deps/sqlite.hpp"
#include "utils/signal.hpp"

#include "modules/dummy.hpp"
#include "modules/philips.hpp"

#include "resources/test.hpp"
#include "resources/query.hpp"
#include "resources/command.hpp"

using namespace iot;
using namespace std;

int main(int argc, char** argv)
{
    auto opts = get_options(argc, argv);

    Env env;
    env.make_modules<
//        modules::Dummy,
        modules::Philips
        >();
   
    ////////////
//    test();
//    return 0;
    ////////////

    env.init_modules();

    if (opts.repl)
    {
        sqlite_repl(*opts.repl);
    }
    else
    {
        cout << "Starting server on port " << opts.port << "...\n";
        Server server{ &env, opts.port };
//        server.add<resources::Test>("test");
        server.add<resources::Query>("query");
        server.add<resources::Command>("cmd");
        server.start();

        while (!globals::signal_quit)
        {}
    }
    
    return 0;
}
