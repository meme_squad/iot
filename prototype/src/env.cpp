#include "env.hpp"

using namespace std;

namespace iot
{
    Module* Env::lookup_module(APIDeviceID const& dev_id)
    {
        return const_cast<Module*>(
                const_cast<Env const*>(this)->lookup_module(dev_id)
                );
    }
    
    Module const* Env::lookup_module(APIDeviceID const& dev_id) const
    {
        for (auto const& m : modules)
        {
            if (m->module_name() == dev_id.major)
            {
                return m.get();
            }
        }

        return nullptr;
    }
    
    void Env::init_modules()
    {
        if (init)
        {
            throw logic_error("double Env init");
        }

        init = true;

        for (auto& m : modules)
        {
            m->load_config();
            m->init_database();
        }
    }
}
