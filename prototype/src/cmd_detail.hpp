#pragma once

#include "device.hpp"
#include <variant>
#include <functional>
#include <optional>

namespace iot::detail
{
    template <typename T>
    std::optional<APIDevice> map(
            APIDevice& dev,
            std::function<std::optional<T>(T)> const& f
            )
    {
        for (Cap& cap : dev.caps)
        {
            if (T* specific_cap0 = std::get_if<T>(&cap))
            {
                if (std::optional<T> specific_cap1 = f(*specific_cap0))
                {
                    *specific_cap0 = *specific_cap1;
                    return dev;
                }
                else
                {
                    return std::nullopt;
                }
            }
            else
            {
                continue;
            }
        }

        return std::nullopt;
    }
}
