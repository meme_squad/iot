#pragma once

#include "deps/srv.hpp"
#include "env.hpp"
#include "resource.hpp"
#include "consts.hpp"

namespace iot 
{
    class Server
    {
        public:
            Server(Env*, int port);
            ~Server();
            void start();

            template <typename T, typename... Path>
            void add(Path&&... path_pieces)
            {
                std::unique_ptr<Resource> res { std::make_unique<T>() };
                std::vector<std::string> path { path_pieces... };
                add(move(res), move(path));
            }

        private:
            void add(std::unique_ptr<Resource>&&, std::vector<std::string>&&);

            Env* _env = nullptr;
            srv::webserver _server;
            std::vector<std::unique_ptr<Resource>> _storage;
    };
}
