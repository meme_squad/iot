#pragma once

#include "device.hpp"
#include "cmd.hpp"
#include "db.hpp"
#include "utils/string.hpp"
#include "utils/vector.hpp"
#include "utils/optional.hpp"

namespace iot
{
    class Module
    {
        public:
            APIDeviceID make_id(string const&) const;
            virtual ~Module() = default;
            virtual string module_name() const = 0;
            virtual void load_config() = 0;
            virtual void init_database() = 0;
            virtual vector<APIDevice> query() const = 0;
            virtual optional<APIDevice> query(APIDeviceID const&) const = 0;
            virtual optional<APIDevice> cmd(APIDeviceID const&, Cmd const&) = 0;
        protected:
            DB open_db() const;
            Path db_filename() const;
            Path cfg_filename() const;
    };
}
