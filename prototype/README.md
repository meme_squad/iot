(Don't expect this to work.)

####Prerequisites
* G++ >= 7.3
* CMake >= 3.9
* SQLite3
* libmicrohttpd >= 0.9.55

####Building
```
cd external/libhttpserver
./bootstrap
cd ../..
mkdir build
cd build
cmake ..
make
```
