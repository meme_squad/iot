if(SQLITE3_INCLUDE_DIRS AND SQLITE3_LIBRARIES)
    set(SQLITE3_FOUND TRUE)

else(SQLITE3_INCLUDE_DIRS AND SQLITE3_LIBRARIES)
    find_path(SQLITE3_INCLUDE_DIRS sqlite3
      /usr/include
      /usr/include/httpserver
      /usr/local/include/
      /usr/local/include/httpserver
      )

  find_library(SQLITE3_LIBRARIES NAMES sqlite3
      PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      )

  if(SQLITE3_INCLUDE_DIRS AND SQLITE3_LIBRARIES)
      set(SQLITE3_FOUND TRUE)
      message(STATUS "Found sqlite3: ${SQLITE3_INCLUDE_DIRS}, ${SQLITE3_LIBRARIES}")
  else(SQLITE3_INCLUDE_DIRS AND SQLITE3_LIBRARIES)
      set(SQLITE3_FOUND FALSE)
    message(STATUS "sqlite3 not found.")
  endif(SQLITE3_INCLUDE_DIRS AND SQLITE3_LIBRARIES)

  mark_as_advanced(SQLITE3_INCLUDE_DIRS SQLITE3_LIBRARIES)

endif(SQLITE3_INCLUDE_DIRS AND SQLITE3_LIBRARIES)
