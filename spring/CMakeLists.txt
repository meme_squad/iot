cmake_minimum_required(VERSION 3.9)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/CMakePCHCompiler)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_STANDARD 17)

project(iod CXX CXXPCH)

add_definitions("-std=c++11")
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
set(CONAN_SYSTEM_INCLUDES ON)
conan_basic_setup()

add_executable(iod 
    src/main.cpp
    src/app.cpp                     src/app.hpp
                                    src/config.hpp
    
    src/client/client.cpp           src/client/client.hpp
    
    src/server/server.cpp           src/server/server.hpp
    src/server/handle_get.cpp       src/server/handle_get.hpp
    src/server/api.cpp              src/server/api.hpp
                                    src/server/apiresult.hpp
                                    src/server/apitask.hpp
    
    src/device/dev.cpp              src/device/dev.hpp
    src/device/devcap.cpp           src/device/devcap.hpp
    src/device/devcmd.cpp           src/device/devcmd.hpp
    src/device/devid.cpp            src/device/devid.hpp
    
    src/env/env_api.cpp             src/env/env_api.hpp
    src/env/env_app.cpp             src/env/env_app.hpp
                                    src/env/env.hpp
                                    src/env/env_impl.hpp
    
    src/hardware/hardware.cpp       src/hardware/hardware.hpp
    src/hardware/hwdummy.cpp        src/hardware/hwdummy.hpp
    src/hardware/hwphilips.cpp      src/hardware/hwphilips.hpp
    src/hardware/hwtplink.cpp	    src/hardware/hwtplink.hpp	
    
    src/util/color.cpp              src/util/color.hpp
    src/util/db.cpp                 src/util/db.hpp
                                    src/util/defaultable.hpp
    src/util/file.cpp               src/util/file.hpp
    src/util/threads.cpp            src/util/threads.hpp
                                    src/util/exception.hpp
                                    src/util/function.hpp
                                    src/util/future.hpp
                                    src/util/json.hpp
                                    src/util/map.hpp
                                    src/util/memory.hpp
    src/util/output.cpp             src/util/output.hpp
                                    src/util/result.hpp
    src/util/rgb.cpp                src/util/rgb.hpp
                                    src/util/set.hpp
                                    src/util/sqlite.hpp
    src/util/string.cpp             src/util/string.hpp
                                    src/util/unreachable.hpp
                                    src/util/variant.hpp
                                    src/util/vector.hpp
	src/util/rgb.cpp                src/util/rgb.hpp
                                    src/util/url.hpp
                                    src/util/wrappers.hpp
    )
target_precompiled_header(iod src/precompile.hpp)
    
#target_link_libraries(iod asan ${CONAN_LIBS})
target_link_libraries(iod ${CONAN_LIBS})
    
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    target_link_libraries(iod stdc++fs ${CONAN_LIBS})
else ()
    target_link_libraries(iod ${CONAN_LIBS})
endif ()

target_include_directories(iod
    PRIVATE 
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        ${CMAKE_CURRENT_SOURCE_DIR}/src/client
        ${CMAKE_CURRENT_SOURCE_DIR}/src/dep
        ${CMAKE_CURRENT_SOURCE_DIR}/src/device
        ${CMAKE_CURRENT_SOURCE_DIR}/src/env
        ${CMAKE_CURRENT_SOURCE_DIR}/src/hardware
        ${CMAKE_CURRENT_SOURCE_DIR}/src/server
        ${CMAKE_CURRENT_SOURCE_DIR}/src/util
    SYSTEM
        ${CMAKE_CURRENT_SOURCE_DIR}/external
    )

if (MSVC)
    target_compile_options(iod
        PUBLIC     
            /permissive
            /W4
            /w14640
            /w14242
            /w14254
            /w14263
            /w14265
            /w14287
            /we4289
            /w14296
            /w14311
            /w14545
            /w14546
            /w14547
            /w14549
            /w14555
            /w14640
            /w14826
            /w14905
            /w14906
            /w14928
    )
    source_group("src"           REGULAR_EXPRESSION "src/.*")
    source_group("src\\client"   REGULAR_EXPRESSION "src/client/.*")
    source_group("src\\device"   REGULAR_EXPRESSION "src/device/.*")
    source_group("src\\env"      REGULAR_EXPRESSION "src/env/.*")
    source_group("src\\hardware" REGULAR_EXPRESSION "src/hardware/.*")
    source_group("src\\server"   REGULAR_EXPRESSION "src/server/.*")
    source_group("src\\util"     REGULAR_EXPRESSION "src/util/.*")
else()
    target_compile_options(iod
        PUBLIC
            -Wall 
            -Wextra 
            -Werror=return-type
            -Werror=switch
            -Wfatal-errors
        
            -Werror
            -Wshadow
            -Wnon-virtual-dtor
            -Wold-style-cast
            -Wcast-align
            -Wunused
            -Woverloaded-virtual
            -Wpedantic
            -Wconversion
            -Wsign-conversion
            -Wmisleading-indentation
            -Wduplicated-cond
            -Wlogical-op
            -Wnull-dereference
            -Wdouble-promotion
            -Wformat=2
            -Wno-parentheses
            # -fsanitize=address
    )
endif()
