On Windows:
    Requirements:
        Python + Pip
        CMake
        Git
        Visual Studio with C++ module

    Setup:
        Install Conan dependency manager
        > pip install conan
        
        Add bincrafters repo to Conan
        > conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
        
        Install dependencies for project
        > cd c:\...\iot\spring
        > mkdir build
        > cd build
        > conan install --build=missing ..
        
        Generate Visual Studio solution (build/iod.sln)
        > cmake ..
        
        Visual Studio should re-run cmake whenever it needs to from this point on.
        
    Modifying:
        Since cmake generates the solution, changes you make to the solution will be lost
        next time cmake runs. Therefore, only make changes through CMakeLists.txt.
        If you want to add files, add them inside the add_executable() clause.        
        
Adding new capabilities:
    Assuming your capability is called x

    device/devcap.hpp
        define struct DevCapX
        add DevCapX to the DevCap variant
        declare void to_json(JSON&, DevCapX const&)
        declare void from_json(JSON const&, DevCapX&)
    
    device/devcap.cpp
        define void to_json(JSON&, DevCapX const&)
        define void from_json(JSON const&, DevCapX&)
        define match overload in void to_json(JSON&, Vector<DevCap> const&)
    
    device/devcmd.hpp
        define DevCmdX
        add DevCmdX to the DevCmd variant

    device/devcmd.cpp
        define Optional<Dev> ApplyDevCmd::operator()(DevCmdX)

    server/api.cpp
        define Optional<JSON> API::try_cmd_x(DevID const&)
            Remember to TRY(try_end()) after parsing last argument
        add { "x", &API::try_cmd_x } to cmd_map in try_dev_cmd
            API name should match internal name, but doesn't have to

    hardware/hw*.cpp
        for any hardware module that understands capability x,
          add functionality to generalize/specialize it between
          an API device (Dev) and a hardware-specific device (probably MyDev)
