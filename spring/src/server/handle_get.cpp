#include "handle_get.hpp"
#include "api.hpp"
#include "output.hpp"
#include "unreachable.hpp"

#define MODULE_ID "SERVER"
#define MODULE_COLOR green

namespace iod {
    
static
Reply reply_ok(JSON const& json) {
    return Reply{ 200, json.dump() };
}

static 
Reply reply_404() {
    return Reply{ 404, "" };
}

static 
Reply reply_devices(Vector<Dev> const& devs) {
    return reply_ok(
        JSON{ { "devices", devs } }
        );
}

static 
Reply reply_error(String const& what) {
    return reply_ok(
        JSON{ {"error", { { "what", what } } } }
        );
}

Reply handle_get(Env* env, Vector<String> const& path) {
    APIOutput result = try_api(env, path);
    
    if (result) {
        return result.value().match(
            [&](Vector<Dev> const& devs) {
                return reply_devices(devs);
            }
        );
    }
    else {
        try {
            switch (result.error()) {
                case APIError::parse_failure:
                    return reply_404();
                
                case APIError::cmd_failure:
                case APIError::backend_timeout:
                    return reply_error("internal error");
                
                case APIError::device_not_found:
                case APIError::hardware_not_found:
                    return reply_error("device not found");
                
                case APIError::interaction_needed:
                    return reply_error("user interaction required (press button on device, etc)");
            }
        }
        catch (std::exception const& e) {
            PRINT_ERROR(e.what());
            throw;
        }

        UNREACHABLE;
    }
}

} //iod
