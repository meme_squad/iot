#pragma once

#include "string.hpp"
#include "vector.hpp"
#include "env.hpp"

namespace iod {

struct Reply {
    unsigned short status_code;
    String body;
};

Reply handle_get(Env*, Vector<String> const& path);

} //iod
