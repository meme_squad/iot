#pragma once

#include "result.hpp"

namespace iod {

enum class APIError {
    parse_failure,
    cmd_failure,
    device_not_found,
    hardware_not_found,
    backend_timeout,
    interaction_needed,
};

template <typename T>
using APIResult = Result<T, APIError>;

} //iod
