#pragma once

#include "memory.hpp"
#include "env.hpp"

namespace iod {

struct ServerImpl;

class Server {
    public:
        explicit Server(Env*);
        ~Server();
        void run();

    private:
        UniquePtr<ServerImpl> _impl;
};

} //iod
