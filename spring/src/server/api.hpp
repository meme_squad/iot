#pragma once

#include "result.hpp"
#include "vector.hpp"
#include "string.hpp"
#include "json.hpp"
#include "env.hpp"
#include "dev.hpp"
#include "variant.hpp"
#include "apiresult.hpp"

namespace iod {

using APIValue = Variant<
    Vector<Dev>
>;

char const* const api_name_delete = "DELETE THIS";

using APIOutput = APIResult<APIValue>;

APIOutput try_api(Env*, Vector<String> const&);

} //iod
