#include "server.hpp"
#include "handle_get.hpp"
#include "string.hpp"
#include "vector.hpp"
#include "output.hpp"
#include "config.hpp"
#include <cpprest/http_listener.h>
#include <cpprest/filestream.h>
#include <boost/system/system_error.hpp>

#define MODULE_ID "SERVER"
#define MODULE_COLOR green

namespace iod {
        
namespace rest {
    using namespace web::http;
    using experimental::listener::http_listener;
}

struct ServerImpl {
    Env* _env;
    rest::http_listener _listener;

    explicit ServerImpl(Env*);
    void handle_get(rest::http_request);
};

Server::Server(Env* env)
    : _impl(make_unique<ServerImpl>(env))
     {
}

Server::~Server() {
    try {
        _impl->_listener.close().wait();
    }
    catch (std::exception const& e) {
        PRINT_ERROR("destructor threw: ", e.what());
    }
    catch (...) {
        PRINT_ERROR("destructor threw unknown exception");
    }
}

void Server::run() {
    // In debug mode, open() throws an error code even on success.
    // So we'll just intercept that.
    try {
        _impl->_listener.open().wait();
    }
    catch (boost::system::system_error &e) {
        if (e.code().value() != 0) {
            throw;
        }
    }
    
    PRINT_NOTICE("listening for requests on port whatever"); //TODO
}
    
ServerImpl::ServerImpl(Env* env)
    : _env(env)
    , _listener(U("http://0.0.0.0:8000")) //TODO port cfg
    {
    auto get = [this](rest::http_request const& x) {
        return handle_get(x);
    };

    _listener.support(rest::methods::GET, get);
}
           
static
Vector<String> split_path(rest::http_request const& message) {
#ifdef _WIN32
    auto path16 = rest::uri::split_path(
        rest::uri::decode(message.relative_uri().path())
        );
    
    Vector<String> path;
    
    for (auto const& s : path16) {
        path.push_back(from_wide(s));
    }
    
    return path;
#else
    return rest::uri::split_path(
        rest::uri::decode(message.relative_uri().path())
        );
#endif
}

void ServerImpl::handle_get(rest::http_request message) {
    PRINT_NOTICE("GET ", message.relative_uri().to_string());
    auto [status_code, body] = iod::handle_get(_env, split_path(message));
	PRINT_NOTICE("GET -> ", status_code, " ", body);

	rest::http_response response;
	response.set_status_code(status_code);
	response.set_body(body, "application/json; charset=utf-8");
    response.headers().add("Access-Control-Allow-Origin", "*");
    message.reply(response);
}

} //iod
