#include "api.hpp"
#include "dev.hpp"
#include "devcmd.hpp"
#include "env_api.hpp"
#include "hardware.hpp"
#include "json.hpp"
#include "map.hpp"
#include "result.hpp"
#include "vector.hpp"
#include "output.hpp"

namespace iod {

class API {
    public:
        API(Env*, Vector<String> const& input);
        APIOutput try_root();
    
    private:
        String const& cur() const;
        bool at_end() const;
        void next();

        template <typename F>
        APIResult<APIFuture> queue_task(DevID const&, F&&);
        
        template <typename F>
        APIResult<Vector<APIFuture>> queue_mass_task(F const&);
        
        APIResult<Hardware*> lookup_hardware(DevID const&);
        APIResult<void> match(String const&);
        APIResult<void> try_end();
        APIResult<bool> try_bool();
        APIResult<int> try_int();
        APIResult<DevID> try_dev_id();
        APIOutput try_cmd(DevID const&, DevCmd const&);
        APIOutput try_root_cmd();
        APIOutput try_root_query();
        //TODO rename stuff
        APIOutput try_dev_cmd(DevID const&);
        APIOutput try_cmd_power(DevID const&);
        APIOutput try_cmd_bri(DevID const&);
		APIOutput try_cmd_col(DevID const&);
		APIOutput try_cmd_auth(DevID const&);
        APIOutput try_cmd_query(DevID const&);
        
    private:
        Env* _env;
        Vector<String> const& _input;
        unsigned _pos;
};
        
API::API(Env* env, Vector<String> const& input)
    : _env(env)
    , _input(input)
    , _pos()
    {
}
        
String const& API::cur() const {
    static String const no_str;

    if (at_end()) {
        return no_str;
    }
    else {
        return _input[_pos];
    }
}

bool API::at_end() const {
    return _pos >= _input.size();
}

void API::next() {
    if (!at_end()) {
        ++_pos;
    }
}

APIResult<void> API::match(String const& lit) {
    if (cur() == lit) {
        next();
        return success();
    }
    else {
        return APIError::parse_failure; 
    }
}

APIResult<DevID> API::try_dev_id() {
    DevID dev_id;
    char const sep = '*';
    
    auto parse = [&]() {
        unsigned i = 0;
        String const& cur = this->cur();
        size_t const end = cur.size();

        while (i < end) {
            if (cur[i] == sep) {
                ++i;
                break;
            }

            dev_id.major.push_back(cur[i]);
            ++i;
        }

        while (i < end) {
            if (cur[i] == sep) {
                return false;
            }

            dev_id.minor.push_back(cur[i]);
            ++i;
        }

        return !dev_id.major.empty()
            && !dev_id.minor.empty();
    };

    if (!parse()) {
        return APIError::parse_failure;
    }

    next();
    return dev_id; 
}

APIOutput try_api(Env* env, Vector<String> const& paths) {
    API p(env, paths);
    return p.try_root();
}

APIResult<bool> API::try_bool() {
    String const& s = cur();
    bool out = false;

    if      (s == "on")    { out = true; }
    else if (s == "off")   { out = false; }
    else if (s == "true")  { out = true; }
    else if (s == "false") { out = false; }
    else {
        return APIError::parse_failure;
    }

    next();
    return out;
}
 
APIResult<int> API::try_int() {
    try {
        int i = std::stoi(cur());
        next();
        return i;
    }
    catch (std::invalid_argument&) {
        return APIError::parse_failure;
    }
    catch (std::out_of_range&) {
        return APIError::parse_failure; 
    }
}

APIResult<void> API::try_end() {
    if (at_end()) {
        return success();
    }
    else {
        return APIError::parse_failure;
    }
}

APIOutput API::try_root() {
    TRY_(match("api"));

    if (cur() == "query") {
        next();
        return try_root_query();
    }
    else if (cur() == "cmd") {
        next();
        return try_root_cmd();
    }
    else {
        return APIError::parse_failure; 
    }
}

template <typename F>
APIResult<APIFuture> API::queue_task(DevID const& dev_id, F&& f) {
    return iod::queue_task(_env, dev_id.major, APICall(std::forward<F>(f)));
}

template <typename F>
APIResult<Vector<APIFuture>> API::queue_mass_task(F const& f) {
    return iod::queue_mass_task(_env, APICall(f));
}

APIResult<Vector<Dev>> try_future(APIFuture& fut) {
    if (std::future_status::ready != fut.wait_for(std::chrono::seconds(5))) {
        return APIError::backend_timeout;
    }
    
    // Combining these two lines caused outcome to throw. Leave alone.
    auto result = *fut.get();
    return result;
}

APIOutput API::try_root_query() {
    TRY_(try_end());
    Vector<Dev> all_devs;
   
    TRY(futs, queue_mass_task( 
        [=](Hardware* hw) {
            return hw->query_all();
        }
    ));

    for (APIFuture& fut : futs) {
   
        LOOP_TRY(devs, try_future(fut)); //TODO LOOP_TRY eats errors
   
        move_append(all_devs, std::move(devs));
  
    }
  

    return all_devs;
}

APIOutput API::try_root_cmd() {
    TRY(dev_id, try_dev_id());
    return try_dev_cmd(dev_id);
}

APIOutput API::try_cmd(DevID const& dev_id, DevCmd const& cmd) {
    TRY(fut, queue_task(dev_id, 
        [=](Hardware* hw) {
            return hw->cmd(dev_id, cmd);
        }
    ));
        
    TRY(devs, try_future(fut));
    return devs;
}

// DEV CMDS ///////////////////////////////////////////////////////////////////

APIOutput API::try_dev_cmd(DevID const& dev_id) {
    using CmdFunc = APIOutput(API::*)(DevID const&);
    
    static Map<String, CmdFunc> const cmd_map {
        { "power", &API::try_cmd_power },
        { "bri"  , &API::try_cmd_bri   },
		{ "col"  , &API::try_cmd_col   },
		{ "auth" , &API::try_cmd_auth   },
        { "query", &API::try_cmd_query },
    };

    if (Optional<CmdFunc> func = lookup(cmd_map, cur())) {
        next();
        return std::invoke(*func, this, dev_id);
    }
    else {
        return APIError::parse_failure;
    }
}

APIOutput API::try_cmd_power(DevID const& dev_id) {
    TRY(on, try_bool());
    TRY_(try_end());
    return try_cmd(
        dev_id, 
        DevCmdPower{ on }
        );
}

APIOutput API::try_cmd_bri(DevID const& dev_id) {
    TRY(bri, try_int());
    TRY_(try_end());
    return try_cmd(
         dev_id, 
         DevCmdBri{ bri }
         );
}

APIOutput API::try_cmd_col(DevID const& dev_id) {
	TRY(r, try_int());
	TRY(g, try_int());
	TRY(b, try_int());
	TRY_(try_end());
	return try_cmd(
		dev_id,
		DevCmdCol(RGB(r, g, b))
	);
}

APIOutput API::try_cmd_auth(DevID const& dev_id) {
	TRY_(try_end());
	return try_cmd(
		dev_id,
		DevCmdAuth()
	);
}

APIOutput API::try_cmd_query(DevID const& dev_id) {
    TRY_(try_end());
    TRY(fut, queue_task(dev_id, 
        [=](Hardware* hw) {
            return hw->query(dev_id);
        }
    ));

    TRY(devs, try_future(fut));
    return devs;
}

} //iod
