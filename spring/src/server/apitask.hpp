// env_types.hpp
//  Extra types related to Env.
#pragma once

#include "future.hpp"
#include "json.hpp"
#include "result.hpp"
#include "hardware.hpp"
#include "function.hpp"
#include "memory.hpp"
#include "apiresult.hpp"
#include "defaultable.hpp"

namespace iod {

using APICall = Function<
    APIResult<Vector<Dev>>(Hardware*)
    >;

// The result is wrapped because MSVC's std::future is
//   deficient and gets hung up on a deleted default constructor.
using APIFuture = Future<Defaultable<
    APIResult<Vector<Dev>>
    >>;

} //iod
