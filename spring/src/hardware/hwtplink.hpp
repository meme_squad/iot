#pragma once

#include "hardware.hpp"
#include "memory.hpp"

namespace iod {

struct HWTplinkImpl;

class HWTplink : public Hardware {
    public:
        HWTplink();
        ~HWTplink();
        HardwareID hw_id() const override;
        void load_config() override;
        void init_database() override;
        APIResult<Vector<Dev>> query_all() const override;
        APIResult<Vector<Dev>> query(DevID const&) const override;
        APIResult<Vector<Dev>> cmd(DevID const&, DevCmd const&) override;

    private:
        UniquePtr<HWTplinkImpl> _impl;
};

} //iod
