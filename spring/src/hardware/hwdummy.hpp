#pragma once

#include "hardware.hpp"
#include "memory.hpp"

namespace iod {

struct HWDummyImpl;

class HWDummy : public Hardware {
    public:
        HWDummy();
        ~HWDummy();
        HardwareID hw_id() const override;
        void load_config() override;
        void init_database() override;
        APIResult<Vector<Dev>> query_all() const override;
        APIResult<Vector<Dev>> query(DevID const&) const override;
        APIResult<Vector<Dev>> cmd(DevID const&, DevCmd const&) override;

    private:
        UniquePtr<HWDummyImpl> _impl;
};

} //iod
