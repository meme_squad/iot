#include "hwdummy.hpp"
#include "db.hpp"
#include "output.hpp"

namespace iod {

#define HW_ID "dummy"
#define MODULE_ID HW_ID
#define MODULE_COLOR cyan
#define TRACE //PRINT_TRACE

struct HWDummyImpl {
    HWDummy& self;
    
    explicit HWDummyImpl(HWDummy& self_) 
        : self(self_) 
        {
    }

    void write_device(Dev const&);
};

HWDummy::HWDummy()
    : _impl(make_unique<HWDummyImpl>(*this))
    {
}

HWDummy::~HWDummy() {
}

HardwareID HWDummy::hw_id() const {
    return HW_ID;
}

void HWDummy::load_config() {
    // Nothing to configure yet.
}

void HWDummy::init_database() {
TRACE;
    DB db(my_db());
    
    db.query("CREATE TABLE IF NOT EXISTS devices (device_id TEXT PRIMARY KEY, power INTEGER, bri INTEGER, r INTEGER, g INTEGER, b INTEGER, auth TEXT)");

    Dev dev_a{
        make_id("dummylight"),
        "normal light",
        { 
            DevCapPower(false), 
            DevCapBri(45, Min(0), Max(254)),
        }
    };

    Dev dev_b{
        make_id("dummycolorlight"),
        "colored light",
        { 
            DevCapPower(true), 
            DevCapBri(128, Min(0), Max(254)),
            DevCapCol(RGB(32,64,128))
        }
    };
    
    Dev dev_c{
        make_id("dummyswitch"),
        "switch",
        { 
            DevCapPower(true), 
        }
    };
    
    Dev dev_d{
        make_id("dummyhub"),
        "hub",
        { 
            DevCapAuth(), 
        }
    };

    _impl->write_device(dev_a);
    _impl->write_device(dev_b);
    _impl->write_device(dev_c);
    _impl->write_device(dev_d);
}

APIResult<Vector<Dev>> HWDummy::query_all() const {
    TRACE;
    Vector<Dev> result;
    
    DB db(my_db());
    SQL const query("SELECT * FROM devices");
    String name, value;
    
    db.query(query, [&](DBRow row) {
        Dev device;
        Optional<DevCapPower> cap_power = failure;
        Optional<DevCapBri>   cap_bri   = failure;
        Optional<DevCapCol>   cap_col   = failure;
        Optional<DevCapAuth>  cap_auth  = failure;
        
        device.id = make_id(""); // Will be set from row.
        
        for (auto [name_, value_] : row) {
            name = name_;
            value = value_;

            if (value.empty()) {
                continue;
            }

            //TODO more typesafe sqlite3 results
            if (name == "device_id") {
                device.id.minor = value;
                device.name = value;
            }
            else if (name == "power") {
                if (!cap_power) {
                    cap_power = DevCapPower{false};
                }

                cap_power.value().now = (value != "0");
            }
            else if (name == "bri") {
                if (!cap_bri) {
                    cap_bri = DevCapBri{0, Min(0), Max(254)};
                }
                
                cap_bri.value().now = std::stoi(value);
            }
            else if (name == "r") {
                if (!cap_col) {
                    cap_col = DevCapCol(RGB());
                }

                cap_col.value().rgb.r = std::stoi(value);
            }
            else if (name == "g") {
                if (!cap_col) {
                    cap_col = DevCapCol(RGB());
                }

                cap_col.value().rgb.g = std::stoi(value);
            }
            else if (name == "b") {
                if (!cap_col) {
                    cap_col = DevCapCol(RGB());
                }

                cap_col.value().rgb.b = std::stoi(value);
            }
            else if (name == "auth") {
                if (!cap_auth) {
                    cap_auth = DevCapAuth();
                }
            }
        }
		
        auto do_cap = [&](auto&& cap) {
            if (cap) {
                device.caps.push_back(cap.value());
            }
        };

        do_cap(cap_power);
        do_cap(cap_bri);
        do_cap(cap_col);
        do_cap(cap_auth);

        result.push_back(std::move(device));
        return SQLITE_OK;
    });
    
    return result;
}

APIResult<Vector<Dev>> HWDummy::query(DevID const& dev_id) const {
    TRACE;
    TRY(devs, query_all());

    for (Dev const& dev : devs) {
        if (dev.id == dev_id) {
            return Vector<Dev>{ dev };
        }
    }

    return APIError::device_not_found;
}

APIResult<Vector<Dev>> HWDummy::cmd(DevID const& dev_id, DevCmd const& cmd) {
    TRACE;
    TRY(devs, query_all());
    
    for (Dev const& old_dev : devs) {
        if (old_dev.id == dev_id) {
            if (Optional<Dev> new_dev = apply_cmd(old_dev, cmd)) {
                _impl->write_device(new_dev.value());
                return Vector<Dev>{ new_dev.value() };
            }
            else {
                return APIError::cmd_failure;
            }
        }
    }

    return APIError::device_not_found;
}

void HWDummyImpl::write_device(Dev const& dev) {
    TRACE;
    String dev_id = dev.id.minor;
    String bri = "''";
    String power = "''";
    String r = "''";
    String g = "''";
    String b = "''";
    String auth = "''";

    match_each(dev.caps,
        [&](DevCapPower const& cap) {
            power = to_string(cap.now);
        },
        [&](DevCapBri const& cap) {
            bri = to_string(cap.now);
        },
		[&](DevCapCol const& cap) {
            r = to_string(cap.rgb.r);
            g = to_string(cap.rgb.g);
            b = to_string(cap.rgb.b);
        },
        [&](DevCapAuth const&) {
            auth = "'x'";
        }
    );

	DB db(self.my_db());
    SQL query(
        "INSERT OR REPLACE INTO devices(device_id,power,bri,r,g,b,auth) VALUES('"
        + dev_id
        + "',"
        + power
        + ","
        + bri
        + ","
        + r
        + ","
        + g
        + ","
        + b
        + ","
        + auth
        + ")"
    );
    db.query(query);
}

} //iod
