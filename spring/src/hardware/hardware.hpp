#pragma once

#include "result.hpp"
#include "dev.hpp"
#include "devcmd.hpp"
#include "json.hpp"
#include "vector.hpp"
#include "apiresult.hpp"

namespace iod {
    
class Hardware {
    public:
        virtual ~Hardware() = default;
        virtual HardwareID hw_id() const = 0;
        virtual void load_config() = 0;
        virtual void init_database() = 0;
        virtual APIResult<Vector<Dev>> query_all() const = 0;
        virtual APIResult<Vector<Dev>> query(DevID const&) const = 0;
        virtual APIResult<Vector<Dev>> cmd(DevID const&, DevCmd const&) = 0;
        
        DevID make_id(String const&) const;
        FilePath my_db() const;
        FilePath my_cfg() const;
};

} //iod
