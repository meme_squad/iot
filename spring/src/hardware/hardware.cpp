#include "hardware.hpp"

namespace iod {

DevID Hardware::make_id(String const& minor) const {
    return DevID(hw_id(), minor);
}

FilePath Hardware::my_db() const {
    return hw_id() + ".sqlite";
}

FilePath Hardware::my_cfg() const {
    return hw_id() + ".cfg";
}

} //iod
