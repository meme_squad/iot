#include "hwphilips.hpp"
#include "db.hpp"
#include "map.hpp"
#include "set.hpp"
#include "file.hpp"
#include "url.hpp"
#include "threads.hpp" //TODO remove me?
#include "client.hpp"
#include "output.hpp"
#include "rgb.hpp"
#include "api.hpp"

#define HW_ID "philips"
#define MODULE_ID HW_ID
#define MODULE_COLOR cyan
#define TRACE if (s_trace) { PRINT_TRACE; }

namespace iod {

namespace {
    static bool s_trace = false; // TODO this isn't ideal
   
    using MyUser = String;
    using MyHubAddr = String;
    using MyNetPath = String;
    using MyDevID = String;
    using MyModelID = String;

    struct MyHub {
        MyHubAddr addr;
    };

    // MyCacheDev
    //  Identifies a module path to a device
    struct MyCacheDev {
        MyHubAddr hub_addr;
        MyNetPath net_path;
    };

    // MyDev
    //  Philips-specific Device type.
    struct MyDev {
        MyDevID unique_id;  // Uniquely identifies a device under Philips.
        MyModelID model_id; // Determines capabilities.
        MyNetPath net_path; // URL on hub to device.
        String name;        // Human-readable device name.
        MyHubAddr hub_addr; // Hub this device is stored on.
        JSON state;         // Contents of 'state' field.

        // try_state
        //  Looks up key in device's state JSON
        //  Returns failure when key does not exist or conversion impossible.
        template <typename T>
        Optional<T> try_state(String const& key) const {
            auto it = state.find(key);

            if (it == state.end()) {
                return failure;
            }
            else {
                try {
                    return success(*it);
                }
                catch (JSONException const&) {
                    PRINT_ERROR("conversion failed for key '", key, "'");
                    return failure;
                }
            }
        }
    };

    // MyStatus
    //  Contains a hub's status (from querying one).
    struct MyHubStatus {
        Vector<MyDev> devs;
    };

    char const* cap_key_power = "on";
    char const* cap_key_bri = "bri";
    char const* cap_key_hue = "hue";
    char const* cap_key_sat = "sat";

    // settable_keys
    //  MyDev state keys that are allowed to be set by the system.
    //  Others are filtered out and ignored.
    static
    Set<String> const settable_keys {
        cap_key_power,
        cap_key_bri,
        cap_key_hue,
        cap_key_sat
    };

    static char const* hub_dev_id_prefix = "hub_";
    static char const* hub_model_id = "<hub>";

    static
    Set<String> const noncacheable_models {
        hub_model_id
    };
} // anonymous namespace

// verify_addr
//  Validates a sting as an IPv4 addr.
//  TODO need a config module that handles these checks, and better.
bool verify_addr(String const& s) {
    TRACE;
    if (s.empty()) {
        return false;
    }

//    for (char c : s) {
//        if (c == '.' || (c >= '0' && c <= '9')) {
//            continue;
//        }
//
//        return false;
//    }

    return true;
}

// HWPhilipsImpl
//  Hidden Philips implementation class
//  All the stuff happens here.
struct HWPhilipsImpl {
    HWPhilips& self;
   
    explicit HWPhilipsImpl(HWPhilips& self_) 
        : self(self_) 
        {
    }
    
    mutable Vector<MyHub> _hubs;
    mutable Map<MyDevID, MyCacheDev> dev_cache;
    bool _trace = false;
    
    void load_config();
    void init_database();
    APIResult<Vector<Dev>> query_all() const;
    APIResult<Vector<Dev>> query(DevID const&) const;
    APIResult<Vector<Dev>> cmd(DevID const&, DevCmd const&);
   
    APIResult<Vector<Dev>> cmd_auth(DevID const&);
    APIResult<Vector<Dev>> query_hub(MyHubAddr const&) const;
    Optional<MyHubAddr> hub_addr_from_dev_id(DevID const&) const;
    Optional<MyDev> query_mydev(DevID const&) const;
    Map<MyDevID,MyDev> query_all_mydevs() const;
    Map<MyDevID,MyDev> query_hub_mydevs(MyHubAddr const&) const;
    Optional<MyDev> get_mydev(MyHubAddr const&, MyNetPath const&) const;
    Vector<MyHub> hubs() const;
    MyDev make_hub_mydev(MyHubAddr const&) const;
    Vector<MyDev> get_hub_mydevs(MyHubAddr const&) const;
    void cache_mydev(MyDev const&) const;
    void clear_hub_cache(MyHubAddr const&) const;
    Optional<JSON> parse_json(String const&) const;
    Optional<MyDev> mydev_from_json(JSON const&, MyHubAddr const&, MyNetPath const&) const;

    void print_device(MyDev const&) const;
    void print_device(Dev const&) const;
    Optional<MyDev> respecialize(Dev const&, MyDev) const;
    Optional<Dev> generalize(MyDev const&) const;
    Optional<MyHubStatus> get_hub_status(MyHubAddr const&, MyUser const&) const;
    Optional<MyUser> auth_with_hub(MyHubAddr const&) const;
    Optional<void> write_device(MyDev const&);
    Optional<void> write_user(MyHubAddr const&, MyUser const&) const;
    Optional<MyUser> read_user(MyHubAddr const&) const;
};

HWPhilips::HWPhilips()
    : _impl(make_unique<HWPhilipsImpl>(*this))
    {
}

HWPhilips::~HWPhilips() {
}

HardwareID HWPhilips::hw_id() const {
    return HW_ID;
}

void HWPhilips::load_config() {
    return _impl->load_config();
}

void HWPhilips::init_database() {
    return _impl->init_database();
}

APIResult<Vector<Dev>> HWPhilips::query_all() const {
    return _impl->query_all();
}

APIResult<Vector<Dev>> HWPhilips::query(DevID const& dev_id) const {
    return _impl->query(dev_id);
}

APIResult<Vector<Dev>> HWPhilips::cmd(DevID const& dev_id, DevCmd const& cmd) {
    return _impl->cmd(dev_id, cmd);
}

void HWPhilipsImpl::load_config() {
    TRACE;
    FilePath const path = self.my_cfg();
    String cmd;

    for (auto [line, line_num] : lines_in(path)) {
        line >> cmd;

        if (cmd == "trace") {
            s_trace = true;
        }
        else if (cmd == "hub") {
            String addr;
            line >> addr; //TODO verify addr
            MyHub hub{ addr };
            _hubs.push_back(hub);
            PRINT_NOTICE("Adding hub at ", addr);
        }
        else {
            PRINT_ERROR(path, ":", line_num, ": unknown cfg command '", cmd, "'");
        }
    }
}

void HWPhilipsImpl::init_database() {
    TRACE;
    DB db(self.my_db());
    db.query("CREATE TABLE IF NOT EXISTS users (hub TEXT PRIMARY KEY, user TEXT)");
}

APIResult<Vector<Dev>> HWPhilipsImpl::query_all() const {
    TRACE;
    Vector<Dev> result;

    for (auto const [unique_id, mydev] : query_all_mydevs()) {
        UNUSED(unique_id);
        LOOP_TRY(dev, generalize(mydev));
        result.push_back(dev);
    }

    return result;
}

APIResult<Vector<Dev>> HWPhilipsImpl::query_hub(MyHubAddr const& hub_addr) const {
    TRACE;
    Vector<Dev> result;

    for (auto const [unique_id, mydev] : query_hub_mydevs(hub_addr)) {
        UNUSED(unique_id);
        LOOP_TRY(dev, generalize(mydev));
        result.push_back(dev);
    }

    return result;
}

Optional<MyDev> HWPhilipsImpl::query_mydev(DevID const& dev_id) const {
    TRACE;
    String const unique_id = dev_id.minor;
    Optional<MyCacheDev> const mycdev = lookup(dev_cache, unique_id);

    if (mycdev) {
        PRINT_NOTICE("cache hit on ", unique_id);
        MyCacheDev const& d = *mycdev;
        return get_mydev(d.hub_addr, d.net_path);
    }
    else {
        PRINT_NOTICE("cache miss on ", unique_id);
        return lookup(query_all_mydevs(), unique_id);
    }
}

Map<MyDevID,MyDev> HWPhilipsImpl::query_hub_mydevs(MyHubAddr const& hub_addr) const {
    TRACE;
    Map<MyDevID,MyDev> result;
    clear_hub_cache(hub_addr);

    for (MyDev const& mydev : get_hub_mydevs(hub_addr)) {
        cache_mydev(mydev);
        result[mydev.unique_id] = mydev;
    }

    return result;
}

Map<MyDevID,MyDev> HWPhilipsImpl::query_all_mydevs() const {
    TRACE;
    Map<MyDevID,MyDev> result;
    dev_cache.clear();

    for (MyHub const& hub : _hubs) {
        result.merge(query_hub_mydevs(hub.addr));
    }

    return result;
}

Optional<MyDev> HWPhilipsImpl::get_mydev(
        MyHubAddr const& hub_addr, 
        MyNetPath const& net_path
        ) const {
    TRACE;
    $TRY(user, read_user(hub_addr));
    Url const url = http / hub_addr / "api" / user / net_path;
    $TRY(rsp, http_get(url));
    $TRY(dev_json, parse_json(rsp));
    return mydev_from_json(dev_json, hub_addr, net_path);
}

//TODO unused
//Vector<MyHub> HWPhilipsImpl::hubs() const {
//    TRACE;
//    //TODO network hub discovery
//    return _hubs;
//}

MyDev HWPhilipsImpl::make_hub_mydev(MyHubAddr const& hub_addr) const {
    TRACE;
    MyDev mydev;
    mydev.unique_id = hub_dev_id_prefix + hub_addr;
    mydev.model_id  = hub_model_id;
    mydev.net_path  = "";
    mydev.name      = "Philips Hub";
    mydev.hub_addr  = hub_addr;
    return mydev;
}

// Returns all devices on hub.
// If hub has no user, returns self as a device.
Vector<MyDev> HWPhilipsImpl::get_hub_mydevs(MyHubAddr const& hub_addr) const {
    TRACE;
    if (Optional<MyUser> user = read_user(hub_addr)) {
        if (Optional<MyHubStatus> status = get_hub_status(hub_addr, *user)) {
//            PRINT_NOTICE("got devices");
            return (*status).devs;
        }
        else {
            PRINT_ERROR("failed to request status from '", hub_addr, "'");
            return {};
        }
    }
    else {
        PRINT_ERROR("failed to query user from '", hub_addr, "'");
        return { make_hub_mydev(hub_addr) };
    }
}

void HWPhilipsImpl::cache_mydev(MyDev const& mydev) const {
    TRACE;
    if (noncacheable_models.count(mydev.model_id)) {
        return;
    }

    MyCacheDev mycdev;
    mycdev.hub_addr = mydev.hub_addr;
    mycdev.net_path = mydev.net_path;
    dev_cache[mydev.unique_id] = mycdev;
    PRINT_NOTICE("cached ", 
        mydev.unique_id, ", ", mydev.hub_addr, ", ", mydev.net_path
        );
}

void HWPhilipsImpl::clear_hub_cache(MyHubAddr const& hub_addr) const {
    TRACE;

    erase_if(dev_cache, [&](auto const& kv) {
        return kv.second.hub_addr == hub_addr;
    });
}

Optional<JSON> HWPhilipsImpl::parse_json(String const& s) const {
    TRACE;
    auto head = [](JSON const& j) {
        if (j.type() == JSON::value_t::array) {
            return j[0];
        }
        else {
            return j;
        }
    };

    try {
        JSON const json = head(JSON::parse(s));
    
        if (json.count("error")) {
            PRINT_ERROR(json["error"].dump());
            return failure;
        }
        
//        PRINT(yellow,
//            json.dump(),
//           );

        return json;
    }
    catch (JSONException const& ex) {
        PRINT_ERROR(ex.what());
        return failure;
    }
}

Optional<MyDev> HWPhilipsImpl::mydev_from_json(
        JSON const& dev_json,
        MyHubAddr const& hub_addr,
        MyNetPath const& net_path
        ) const {
    TRACE;

    try {
        MyDev mydev;
        mydev.unique_id = dev_json["uniqueid"];
        mydev.model_id  = dev_json["modelid"];
        mydev.hub_addr  = hub_addr;
        mydev.net_path  = net_path;
        mydev.name      = dev_json["name"];
        mydev.state     = dev_json["state"]; 
        // TODO could filter mydev.state for modifiable keys here
        return mydev;
    }
    catch (JSONException const& ex) {
        PRINT_ERROR(ex.what());
        return failure;
    }
}

APIResult<Vector<Dev>> HWPhilipsImpl::query(DevID const& dev_id) const {
    TRACE;
    auto go = [&]() -> Optional<Dev> {
        $TRY(mydev, query_mydev(dev_id));
        $TRY(dev, generalize(mydev));
        return dev;
    };

    if (Optional<Dev> dev = go()) {
        return Vector<Dev>{ *dev };
    }
    else {
        PRINT_ERROR("device_not_found: ", dev_id.minor);
        return APIError::device_not_found;
    }
}

Optional<MyHubAddr> HWPhilipsImpl::hub_addr_from_dev_id(DevID const& dev_id) const {
    TRACE;
   
    TRY(addr, pop_prefix(dev_id.minor, hub_dev_id_prefix));

    for (MyHub const& hub : _hubs) {
        if (hub.addr == addr) {
            return addr;
        }
    }

    return failure;
}

APIResult<Vector<Dev>> HWPhilipsImpl::cmd(DevID const& dev_id, DevCmd const& cmd) {
    TRACE;

    if (cmd.is<DevCmdAuth>())
    {
        return cmd_auth(dev_id);
    }
    else
    {
        auto old_mydev_ = query_mydev(dev_id);

        if (!old_mydev_) {
            PRINT_ERROR("device_not_found: ", dev_id.minor);
            return APIError::device_not_found;
        }

        MyDev const& old_mydev = old_mydev_.value();
        
        auto go = [&]() -> Optional<Dev> {
            $TRY(old_dev, generalize(old_mydev));
            $TRY(new_dev, apply_cmd(old_dev, cmd));
            $TRY(new_mydev, respecialize(new_dev, old_mydev));
            
            if (write_device(new_mydev)) {
                return new_dev;
            }
            else {
                PRINT_ERROR("failed to write ", to_string(dev_id));
                return failure;
            }
        };

        if (Optional<Dev> result = go()) {
            return Vector<Dev>{ result.value() };
        }
        else {
            PRINT_ERROR("cmd_failure");
            return APIError::cmd_failure;
        }
    }
}

APIResult<Vector<Dev>> HWPhilipsImpl::cmd_auth(DevID const& dev_id) {
    TRACE;

    // Dev id is user input, and here it's giving us an address.
    // Must be sanitized.
    // TODO sanitize all fields that come from user.
    Optional<MyHubAddr> hub_addr = hub_addr_from_dev_id(dev_id);

    if (!hub_addr)
    {
        return APIError::device_not_found;
    }

    // If already authed ok, hub is no longer a device.
    if (read_user(*hub_addr))
    {
        return APIError::device_not_found;
    }

    if (Optional<MyUser> user = auth_with_hub(*hub_addr))
    {
        // Return all devices now, plus a deleted hub device.
        APIResult<Vector<Dev>> result = query_all();

        if (result)
        {
            Vector<Dev> better_result = result.value();
            Dev delete_hub;
            delete_hub.id = dev_id;
            delete_hub.name = api_name_delete;
            better_result.push_back(delete_hub);
            return std::move(better_result);
        }
        else
        {
            return result;
        }
    }
    else
    {
        return APIError::interaction_needed;
    }
}

//TODO unused
//void HWPhilipsImpl::print_device(MyDev const& mydev) const {
//    TRACE;
//    safe_print(
//		mydev.unique_id, "\n",
//        mydev.net_path, "\n",
//        mydev.state.dump(), "\n"
//	    );
//}

void HWPhilipsImpl::print_device(Dev const& dev) const {
    TRACE;
	safe_print(dev.id.minor, "\n");

    match_each(dev.caps,
        [](DevCapPower const& cap) {
            safe_print("CapPower{", cap.now, "}\n");
        },
        [](DevCapBri const& cap) {
			safe_print("CapBri{", 
                cap.min, ",",
				cap.max, ",",
				cap.now, "}\n"
                );
        },
		[](DevCapCol const& cap) {
			safe_print("CapCol{", 
                cap.rgb.r, ",",
				cap.rgb.g, ",",
				cap.rgb.b, "}\n"
                );
		},
        [](DevCapAuth const&) {
            safe_print("CapAuth{}\n");
        }
    );
}

// respecialize
//  Attempt to merge an abstract Dev's changes on top of a local MyDev.
Optional<MyDev> HWPhilipsImpl::respecialize(Dev const& dev, MyDev my_dev) const {
    TRACE;
    // Convert each cap back into JSON state.
    match_each(dev.caps,
        [&](DevCapPower const& cap) {
            my_dev.state[cap_key_power] = cap.now;
        },
        [&](DevCapBri const& cap) {
            my_dev.state[cap_key_bri] = cap.now;
        },
		[&](DevCapCol const& cap) {
			// TODO chec this logic
			HSL hsl = rgb_to_hsl(cap.rgb);
			my_dev.state[cap_key_hue] = static_cast<int>(hsl.h / 360.0f * 65535);
			my_dev.state[cap_key_sat] = static_cast<int>(hsl.s * 255);
			// TODO could set bri here from l but maybe not
		},
        [&](DevCapAuth const& cap) {
            // TODO
            UNUSED(cap);
        }
    );
    
    return my_dev;
}

Optional<Dev> HWPhilipsImpl::generalize(MyDev const& my_dev) const {
    TRACE;
    Dev dev{ 
        self.make_id(my_dev.unique_id), 
        my_dev.name,
        Vector<DevCap>{} 
    };

    // TODO more declarative list of models and their caps
    if (my_dev.model_id == "LWB014") {
        $TRY(power_now, my_dev.try_state<bool>(cap_key_power));
        $TRY(bri_now,   my_dev.try_state<int>(cap_key_bri));
        DevCapPower power(power_now);
        DevCapBri bri(bri_now, Min(0), Max(255));
        dev.caps = { power, bri };
    }
    else if (my_dev.model_id == "LCT011") {
        $TRY(power_now, my_dev.try_state<bool>(cap_key_power));
        $TRY(bri_now,   my_dev.try_state<int>(cap_key_bri));
		$TRY(hue_now,   my_dev.try_state<int>(cap_key_hue));
		$TRY(sat_now,   my_dev.try_state<int>(cap_key_sat));
		DevCapPower power(power_now);
        DevCapBri bri(bri_now, Min(0), Max(255));
        dev.caps = { power, bri };
		HSL hsl = { //TODO more math
           static_cast<float>(hue_now) * 360.0f / 65535.0f, 
           static_cast<float>(sat_now) / 255.0f, 
           static_cast<float>(bri_now) / 255.0f 
        };
		DevCapCol col = { hsl_to_rgb(hsl) };
		dev.caps = { power, bri, col };
    }
    else if (my_dev.model_id == hub_model_id) {
        dev.caps = { DevCapAuth() };
    }
    else {
        PRINT_ERROR("unhandled model '", my_dev.model_id, "'");
        return failure;
    }
    
    return dev;
}

Optional<MyHubStatus> HWPhilipsImpl::get_hub_status(
        MyHubAddr const& hub_addr, 
        MyUser const& user
        ) const {
    TRACE;
    MyHubStatus result;

    Url const url = http / hub_addr / "api" / user;
    $TRY(rsp, http_get(url));
    $TRY(j, parse_json(rsp));
    
    try {
        JSON const lights = j["lights"];
    
//        PRINT(yellow,
//           lights.dump(),
//           );

        String const base_path = "lights/";

        for (auto it = lights.begin(); it != lights.end(); ++it) {
            LOOP_TRY(mydev, 
                mydev_from_json(*it, hub_addr, base_path + it.key()));
            result.devs.push_back(std::move(mydev));
        }

        return result;
    }
    catch (JSONException const& ex) {
        PRINT_ERROR(ex.what());
        return failure;
    }
}

Optional<MyUser> HWPhilipsImpl::auth_with_hub(MyHubAddr const& hub_addr) const {
    TRACE;
    String const msg = R"(
    {
        "devicetype":"meme_team"
    })";
   
    Url const url = http / hub_addr / "api";

    TRY(rsp, http_post(url, msg));
    TRY(json, parse_json(rsp));

    try {
        MyUser user = json["success"]["username"];
        $TRY_(write_user(hub_addr, user));
        return user;
    }
    catch (JSONException const& ex) {
        PRINT_ERROR(ex.what());
        return failure;
    }
}

// TODO 
//  Client will get incorrect status if you do stuff while power is off.
//  The power-off check/filter should be done earlier.
Optional<void> HWPhilipsImpl::write_device(MyDev const& my_dev) {
    TRACE;
    $TRY(user, read_user(my_dev.hub_addr));
    Url const url = http / my_dev.hub_addr 
                  / "api" / user / my_dev.net_path / "state";
       
    // If power is off, we can't set any other state keys.
    bool const power_off = [&]() {
        Optional<bool> power =
            my_dev.try_state<bool>(cap_key_power);

        if (power) {
            return !*power;
        }
        else {
            return false;
        }
    }();

    JSON state_update;

    for (auto [key, value] : my_dev.state.items()) {
        if (!contains(settable_keys, key)) {
            continue;
        }

        if (power_off && key != cap_key_power) {
            continue;
        }

        state_update[key] = value;
    }

	$TRY(rsp, http_put(url, state_update.dump()));
    $TRY(rsp_json, parse_json(rsp));
    UNUSED(rsp_json);
//	PRINT(yellow,
//		state_update.dump(), 
//        rsp_json.dump(),
//		);
    return success();
}

Optional<void> HWPhilipsImpl::write_user(
        MyHubAddr const& hub_addr, 
        MyUser const& user
        ) const {
    TRACE;
    try {
        DB db(self.my_db());
        SQL const query(
            "INSERT INTO users (hub, user) VALUES('" 
            + hub_addr + "','" + user + "')"
            );
        db.query(query);
        return success();
    }
    catch (SQLException const& e) {
        PRINT_ERROR(e.what());
        return failure;
    }
}

Optional<MyUser> HWPhilipsImpl::read_user(MyHubAddr const& hub_addr) const {
    TRACE;
    Optional<MyUser> result = failure;
    DB db(self.my_db());
    SQL const query(
        "SELECT user FROM users WHERE hub='" + hub_addr + "'"
        );
    
    try {
        db.query(query, [&result](DBRow row) {
            for (auto&& item : row) {
                if (strcmp(item.name, "user") == 0) {
                    result = MyUser(item.value);
                    return SQLITE_ABORT;
                }
            }
            
            return SQLITE_OK;
        });
    }
    catch (SQLException const& ex) {
        PRINT_ERROR(ex.what());
        return failure;
    }
    
    if (!result) {
        PRINT_ERROR("read nothing");
        return failure;
    }

    PRINT_NOTICE(hub_addr, " -> ", result.value());
    return result;
}


//    bool delete_user(MyUser const& user, MyUser const& user_del) {
//        URL const url(
//            url_root()
//          + "/" 
//          + user
//          + "/config/whitelist/" 
//          + user_del
//            );
//        cpr::Response const r = req_delete(url);
//        JSON const j = first(r.text);
//        return check(j);
//    }

} //iod
