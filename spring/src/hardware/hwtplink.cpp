#include "hwtplink.hpp"
#include "db.hpp"
#include "output.hpp"
#include "file.hpp"
#include "url.hpp"
#include "client.hpp"

#define HW_ID "tplink"
#define MODULE_ID HW_ID
#define MODULE_COLOR cyan
#define TRACE PRINT_TRACE
/*
Optional<void> HWPhilipsImpl::write_user(
		MyHubAddr const& hub_addr,
		MyUser const& user
		) const {
	TRACE;
	try {
		DB db(self.my_db());
		SQL const query(
			"INSERT INTO users (hub, user) VALUES('"
			+ hub_addr + "','" + user + "')"
			);
		db.query(query);
		return success();
	}
	catch (SQLException const& e) {
		PRINT_ERROR(e.what());
		return failure;
	}
}
*/
namespace iod {
		
namespace {
    using MyToken = String;
    using MyUser = String;

    struct MyDev {
        String deviceName;
        String hwId;
        String deviceId;
        String deviceModel;
        int status;
    };
    
    static String const appServerUrl = "wap.tplinkcloud.com";
}

Optional<JSON> parse_json(String const& s)  {
	TRACE;
    PRINT_NOTICE(s);

	try {
		JSON const json = JSON::parse(s);
		int error = json["error_code"];
		if (error != 0)
		{
			return failure;
		}

		return json;
	}
	catch (JSONException const& ex) {
		PRINT_ERROR(ex.what());
		return failure;
	}
}

struct HWTplinkImpl {
    HWTplink& self;
	MyUser cfg_user;
	String cfg_uuid;
	String cfg_password;

	Optional<MyDev> respecialize(Dev const& dev, MyDev my_dev) const {
		TRACE;
		match_each(dev.caps,
			[&](DevCapPower const& cap) {
				my_dev.status = cap.now;
			},
				[&](auto const&) {
				std::abort();
			}
		);

		return my_dev;
	}
	template <typename T>
	Optional<T> try_json(JSON const& json, String const& key) const {
		auto it = json.find(key);

		if (it == json.end()) {
			return failure;
		}
		else {
			try {
				return success(*it);
			}
			catch (JSONException const&) {
				PRINT_ERROR("conversion failed for key '", key, "'");
				return failure;
			}
		}
	}

	Optional<Dev> generalize(MyDev const& my_dev) const {
		TRACE;
		Dev dev{
			self.make_id(my_dev.deviceId),
			my_dev.deviceName,
			Vector<DevCap>{}
		};

		// TODO more declarative list of models and their caps
		if (my_dev.deviceModel == "HS110(US)") {
			
			DevCapPower power(my_dev.status);
			dev.caps = {power};
			/*
			$TRY(power_now, my_dev.try_state<bool>(cap_key_power));
			$TRY(bri_now, my_dev.try_state<int>(cap_key_bri));
			DevCapPower power(power_now);
			DevCapBri bri(bri_now, Min(0), Max(255));
			dev.caps = { power, bri };
			*/
		}
		else {
			PRINT_ERROR("unhandled model '", my_dev.deviceModel, "'");
			return failure;
		}

		return dev;
	}


	Optional<MyToken> get_token() {
		//returns the key

		return failure;
	}
	Optional<void> write_device(MyDev mydev) {
		//Need this
		String status = to_string(mydev.status);
		String requestData = "{\"system\":{\"set_relay_state\":{\"state\":" + status + "}}}";
		JSON body =
		{
			{"method", "passthrough"},
			{"params", {
				{"deviceId", mydev.deviceId},
				{"requestData", requestData},
			}},
		};
		TRY(token, read_token(cfg_user));
		Url url = https/appServerUrl / ("?token=" + token);
		TRY_(http_post(url, body.dump()));
        return success();
		
	}
	Optional<void> write_token(MyUser user, MyToken token) {
		//writes to database
			TRACE;
			try {
				DB db(self.my_db());
				SQL const query(
					"INSERT INTO users (user, token) VALUES('"
					+ user + "','" + token + "')"
				);
				db.query(query);
				return success();
			}
			catch (SQLException const& e) {
				PRINT_ERROR(e.what());
				return failure;
			}
	}

//curl --request POST "https://wap.tplinkcloud.com" --data '{"method":"login", "params": {"appType": "Kasa_Android", "cloudUserName":"chris.pretti@yahoo.com", "cloudPassword":"password1", "terminalUUID":"dc53a2cc-f644-46b0-b3f9-fc56bf66f970" }}' --header "Content-Type: application/json"
	Optional<MyToken> read_token(MyUser) const{
		//reads to database
		TRACE;
        static Optional<MyToken> s_token = failure;

        if (s_token)
        {
            return s_token;
        }
		
        Url url = https / appServerUrl;
        JSON body =
        {
            {"method", "login"},
            {"params",
                {
                    {"appType", "Kasa_Android"},
                    {"cloudUserName", cfg_user},
                    {"cloudPassword", cfg_password},
                    {"terminalUUID",  cfg_uuid}
                }
            }
        };

        TRY(result,http_post(url, body.dump()));
        TRY(json, parse_json(result));

        try
        {
            s_token = json["result"]["token"];
            return s_token;
        }
        catch (JSONException const& ex) 
        {
            PRINT_ERROR(ex.what());
            return failure;
        }

//		//This is hardcoded, Benis
//		return "824489e8-A3l05QgFAMzpONhbo6OIUNb";
//		Optional<MyToken> result = failure;
//		DB db(self.my_db());
//		SQL const query(
//			"SELECT token FROM tokens WHERE user='" + user + "'"
//		);
//		try {
//			db.query(query, [&result](DBRow row) {
//				for (auto&& item : row) {
//					if (strcmp(item.name, "token") == 0) {
//						result = MyToken(item.value);
//						return SQLITE_ABORT;
//					}
//				}
//
//				return SQLITE_OK;
//			});
//		}
//		catch (SQLException const& ex) {
//			PRINT_ERROR(ex.what());
//			return failure;
//		}
//		if (!result) {
//			PRINT_ERROR("read nothing");
//			return failure;
//		}
//
//		PRINT_NOTICE(user, " -> ", result.value());
//		return result;


	}

    explicit HWTplinkImpl(HWTplink& self_) 
        : self(self_) 
        {
    }

	void init_database() {
		TRACE;
		DB db(self.my_db());
		db.query("CREATE TABLE IF NOT EXISTS tokens (user TEXT PRIMARY KEY, token TEXT)");
		PRINT_NOTICE("N");
	}

    Optional<int> query_sysinfo(String const& mydev_id, 
            MyToken const& token) const {
		String request_data = "{\"system\":{\"get_sysinfo\":null}}";
		JSON body =
		{
			{"method", "passthrough"},
			{"params", {
				{"deviceId", mydev_id},
				{"requestData", request_data},
			}},
		};
		Url url = https / appServerUrl / ("?token=" + token);
		TRY(result, http_post(url, body.dump()));
        TRY(json, parse_json(result));
        //TODO use a json get thing IMPROVE THIS
        String s = json.at("result").at("responseData");
        JSON response_data = JSON::parse(s);
        return response_data.at("system").at("get_sysinfo").at("relay_state");
    }
	
    Optional<Vector<MyDev>> query_mydevs() const {
		TRY(token, read_token(cfg_user));
        Url url = https / appServerUrl / ("?token=" + token);
        JSON body =
        {
            {"method", "getDeviceList"}

        };
        TRY(result,http_post(url, body.dump()));
        TRY(json, parse_json(result));
        JSON const device_list = json.at("result").at("deviceList");
        Vector<MyDev> mydevs;

        for (auto const& device : device_list)
        {
            MyDev mydev;
            mydev.deviceId = device.at("deviceId");
            mydev.hwId = device.at("hwId");
            mydev.deviceModel = device.at("deviceModel");
            mydev.deviceName = device.at("deviceName");
            LOOP_TRY(status, query_sysinfo(mydev.deviceId, token));
			mydev.status = status;
            mydevs.push_back(mydev);
        }

        return mydevs;
    };
	
	APIResult<Vector<Dev>> query_all() const {
		auto result = query_mydevs();

        if (result)
        {
            Vector<Dev> devs;

            for (MyDev const& mydev : result.value())
            {
                LOOP_TRY(dev, generalize(mydev));
                devs.push_back(dev);
            }

            return devs;
        }
        else
        {
            PRINT_NOTICE("cmd_failure");
            return APIError::cmd_failure;
        }
	}

	APIResult<Vector<Dev>> query(DevID const& dev_id) const {
		auto result = query_mydevs();

        if (result)
        {
            for (MyDev const& mydev : result.value())
            {
                if (mydev.deviceId == dev_id.minor)
                {
                    auto dev = generalize(mydev);

                    if (!dev)
                    {
                        PRINT_NOTICE("cmd_failure");
                        return APIError::cmd_failure;
                    }
                    else
                    {
                        return Vector<Dev>{ dev.value() };
                    }
                }
            }

            PRINT_NOTICE("cmd_failure");
            return APIError::device_not_found;
        }
        else
        {
            PRINT_NOTICE("cmd_failure");
            return APIError::cmd_failure;
        }
		return APIError::cmd_failure;
	}
	APIResult<Vector<Dev>> cmd(DevID const& dev_id, DevCmd const& cmd) {
		TRACE;

		auto old_mydev_ = query_mydev(dev_id);

		if (!old_mydev_) {
			PRINT_ERROR("device_not_found: ", dev_id.minor);
			return APIError::device_not_found;
		}

		MyDev const& old_mydev = old_mydev_.value();

		auto go = [&]() -> Optional<Dev> {
			$TRY(old_dev, generalize(old_mydev));
			$TRY(new_dev, apply_cmd(old_dev, cmd));
			$TRY(new_mydev, respecialize(new_dev, old_mydev));

			if (write_device(new_mydev)) {
				return new_dev;
			}
			else {
				PRINT_ERROR("failed to write ", to_string(dev_id));
				return failure;
			}
		};

		if (Optional<Dev> result = go()) {
			return Vector<Dev>{ result.value() };
		}
		else {
			PRINT_ERROR("cmd_failure");
			return APIError::cmd_failure;
		}

	}

	void load_config() {
		TRACE;
		FilePath const path = self.my_cfg();
        PRINT_NOTICE(path);

		//TODO proper cfg options
		
//		auto lines = lines_in(path);
//		auto it = lines.begin();
		
		cfg_user = "chris.pretti@yahoo.com";
		cfg_password = "password1";
		cfg_uuid = "dc53a2cc-f644-46b0-b3f9-fc56bf66f970";

		assert(!cfg_user.empty());
		assert(!cfg_password.empty());
		assert(!cfg_uuid.empty());
	}

	Optional<MyDev> query_mydev(DevID const&)const {
		
		TRACE;
		/*
		Url const url = http / hub_addr / "api" / user / net_path;
		$TRY(rsp, http_get(url));
		$TRY(dev_json, parse_json(rsp));
		return mydev_from_json(dev_json, hub_addr, net_path);
		*/
		MyDev mydev;
		mydev.deviceName = "Wi-Fi Smart Plug With Energy Monitoring";
		//mydev.status = 1;
		mydev.deviceModel = "HS110(US)";
//		mydev.deviceId = "800608B8A0ABEE9CB2C45FA8147034A61A565012"; old one
        mydev.deviceId = "80069C11045EF8CE6375705D6BF387F018D0F16E";
		mydev.hwId = "60FF6B258734EA6880E186F8C96DDC61";
		return mydev;
	}
	

    void write_device(Dev const&);
};

HWTplink::HWTplink()
    : _impl(make_unique<HWTplinkImpl>(*this))
    {
}

HWTplink::~HWTplink() {
}

HardwareID HWTplink::hw_id() const {
    return HW_ID;
}





void HWTplink::load_config() {
	return _impl->load_config();
}

void HWTplink::init_database() {
	return _impl->init_database();
}

APIResult<Vector<Dev>> HWTplink::query_all() const {
	return _impl->query_all();
}

APIResult<Vector<Dev>> HWTplink::query(DevID const& dev_id) const {
	return _impl->query(dev_id);
}

APIResult<Vector<Dev>> HWTplink::cmd(DevID const& dev_id, DevCmd const& cmd) {
	return _impl->cmd(dev_id, cmd);
}







} //iod
