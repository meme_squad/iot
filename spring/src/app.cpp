#include "app.hpp"
#include "env_app.hpp"
#include "server.hpp"
#include "file.hpp"
#include "config.hpp"
#include "output.hpp"

#include "hwdummy.hpp"
#include "hwphilips.hpp"
#include "hwtplink.hpp"

#define MODULE_ID "app"

namespace iod {

struct AppConfig
{
    bool hw_dummy = false;
    bool hw_philips = false;
    bool hw_tplink = false;
};

AppConfig read_app_config()
{
    AppConfig cfg{};
    String const path = config::app_cfg_path;
    
    String cmd;

    for (auto [line, line_num] : lines_in(path)) {
        line >> cmd;

        if (cmd == "hw") {
            String hwid;
            line >> hwid;

            if (hwid == "dummy") {
                cfg.hw_dummy = true;
            }
            else if (hwid == "philips") {
                cfg.hw_philips = true;
            }
            else if (hwid == "tplink") {
                cfg.hw_tplink = true;
            }
            else {
                PRINT_ERROR(path, ":", line_num, ": unknown hwid '", hwid, "'");
            }
        }
        else {
            PRINT_ERROR(path, ":", line_num, ": unknown command '", cmd, "'");
        }
    }

    return cfg;
}

void app() {
    AppConfig const cfg = read_app_config();

    OpaquePtr<Env> env = new_env();

    if (cfg.hw_dummy) {
        add_hardware<HWDummy>(env.get());
    }
    
    if (cfg.hw_philips) {
        add_hardware<HWPhilips>(env.get());
    }
    
    if (cfg.hw_tplink) {
        add_hardware<HWTplink>(env.get());
    }
    
    Server srv(env.get());
    srv.run();

    // Blocks until killed.
    env_loop(env.get());
}

} //iod

