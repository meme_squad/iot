#pragma once

#include "result.hpp"
#include "dev.hpp"
#include "variant.hpp"
#include "rgb.hpp"

namespace iod {
    
struct DevCmdPower {
    bool on;
};

struct DevCmdBri {
    int bri;
};

struct DevCmdCol {
	RGB rgb;

    DevCmdCol(RGB rgb_)
        : rgb(rgb_)
    {}
};

struct DevCmdAuth {
};

using DevCmd = Variant<
    DevCmdPower,
    DevCmdBri,
	DevCmdCol,
    DevCmdAuth
    >;

Optional<Dev> apply_cmd(Dev const&, DevCmd const&);

} //iod
