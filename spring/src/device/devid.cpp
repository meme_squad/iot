#include "devid.hpp"

namespace iod {

bool operator==(DevID const& a, DevID const& b) {
    return a.major == b.major
        && a.minor == b.minor;
}

bool operator!=(DevID const& a, DevID const& b) {
    return !(a == b);
}

String to_string(DevID const& dev_id) {
    return dev_id.major + dev_id.sep + dev_id.minor;
}

} //iod

