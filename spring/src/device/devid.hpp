#pragma once

#include "string.hpp"

namespace iod {

using HardwareID = String;

struct DevID {
    static char const sep = '*';
    HardwareID major;
    String     minor;

    DevID() = default;
    DevID(HardwareID const& major_, String const& minor_) 
        : major(major_)
        , minor(minor_)
        {
    }
};

bool operator==(DevID const&, DevID const&);
bool operator!=(DevID const&, DevID const&);
String to_string(DevID const&);

} //iod
