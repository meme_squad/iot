#include "devcap.hpp"

namespace iod {

void to_json(JSON& j, const DevCapPower& cap) {
    j = JSON {
        {"now", cap.now}
    };
}

void from_json(const JSON& j, DevCapPower& cap) {
    j.at("now").get_to(cap.now);
}

void to_json(JSON& j, const DevCapBri& cap) {
    j = JSON {
        {"min", cap.min}, 
        {"max", cap.max}, 
        {"now", cap.now}
    };
}

void to_json(JSON& j, const DevCapAuth&) {
    j = JSON {};
}


void from_json(const JSON& j, DevCapBri& cap) {
    j.at("min").get_to(cap.min);
    j.at("max").get_to(cap.max);
    j.at("now").get_to(cap.now);
}

void to_json(JSON& j, const DevCapCol& cap) {
	j = JSON{
		{"r", cap.rgb.r},
		{"g", cap.rgb.g},
		{"b", cap.rgb.b}
	};
}

void from_json(const JSON& j, DevCapCol& cap) {
	j.at("r").get_to(cap.rgb.r);
	j.at("g").get_to(cap.rgb.g);
	j.at("b").get_to(cap.rgb.b);
}

void from_json(const JSON&, DevCapAuth&) {
}


void to_json(JSON& j, Vector<DevCap> const& caps) {
    match_each(caps,
        [&](DevCapPower const& cap) {
            j["power"] = cap;
        },
        [&](DevCapBri const& cap) {
            j["bri"] = cap;
        },
		[&](DevCapCol const& cap) {
			j["col"] = cap;
		},
        [&](DevCapAuth const& cap) {
            j["auth"] = cap;
        }
    );
}

} //iod

