#pragma once

#include "vector.hpp"
#include "variant.hpp"
#include "json.hpp"
#include "rgb.hpp"
#include "wrappers.hpp"

namespace iod {

struct DevCapPower {
    bool now = false;

    DevCapPower() = default;
    DevCapPower(bool now_)
        : now(now_)
    {}
};

struct DevCapBri {
    int now = 0;
    int min = 0;
    int max = 0;

    DevCapBri() = default;
    DevCapBri(int now_, Min<int> min_, Max<int> max_)
        : now(now_)
        , min(min_)
        , max(max_)
    {}
};

struct DevCapCol {
	RGB rgb;

    DevCapCol() = default;
    DevCapCol(RGB rgb_)
        : rgb(rgb_)
    {}
};

struct DevCapAuth {
    DevCapAuth() {}
};


using DevCap = Variant<
    DevCapPower,
    DevCapBri,
	DevCapCol,
    DevCapAuth
    >;

void to_json(JSON&, DevCapPower const&);
void from_json(JSON const&, DevCapPower&); 

void to_json(JSON&, DevCapBri const&);
void from_json(JSON const&, DevCapBri&); 

void to_json(JSON&, DevCapCol const&);
void from_json(JSON const&, DevCapCol&);

void to_json(JSON&, DevCapAuth const&);
void from_json(JSON const&, DevCapAuth&);

void to_json(JSON&, Vector<DevCap> const&);

} //iod

