#include "devcmd.hpp"
#include "function.hpp"

namespace iod {

// map_cap_impl
//  Applies a function to the cap of type C in Dev's caps.
//  Function<> is only used in order to be able to match on the signature.
template <typename C>
Optional<Dev> map_cap_impl(Dev& dev, Function<Optional<C>(C)> const& f) {
    for (DevCap& cap : dev.caps) {
        if (cap.is<C>()) {
            C& c0 = cap.get<C>();
            TRY(c1, f(c0));
            c0 = c1;
            return dev;
        }
        else {
            continue;
        }
    }

    return failure;
}

// ApplyDevCmd
//  Visitor type for applying DevCmds to Devs.
//  DevCmds are mapped over their matching DevCaps.
struct ApplyDevCmd {
    Dev dev;

    template <typename F>
    Optional<Dev> map_cap(F const& f) {
        // This exploits the deduction guides of std::function.
        // Using the Function alias doesn't work.
        return map_cap_impl(dev, std::function(f));
    }

    /* Template for a regular cap-mapping function:
    Optional<Dev> operator()(CMDTYPE cmd) {
        return map_cap([&](CAPTYPE cap) -> Optional<CAPTYPE> {
            Modify cap using cmd and return cap.
            If this is not possible, return failure.
        });
    }
    */

    Optional<Dev> operator()(DevCmdPower cmd) {
        return map_cap([&](DevCapPower cap) -> Optional<DevCapPower> {
            cap.now = cmd.on;
            return cap;
        });
    }
    
    Optional<Dev> operator()(DevCmdBri cmd) {
        return map_cap([&](DevCapBri cap) -> Optional<DevCapBri> {
            if (cmd.bri < cap.min || cmd.bri > cap.max) {
                return failure;
            }
                
            cap.now = cmd.bri;
            return cap;
        });
    }

	Optional<Dev> operator()(DevCmdCol cmd) {
		return map_cap([&](DevCapCol cap) -> Optional<DevCapCol> {
			cap.rgb = cmd.rgb;
			return cap;
		});
	}

    Optional<Dev> operator()(DevCmdAuth) {
        return dev;
    };
};
    
Optional<Dev> apply_cmd(Dev const& dev, DevCmd const& cmd) {
    return apply_visitor(ApplyDevCmd{dev}, cmd);
}

} //iod
