#include "dev.hpp"

namespace iod {

void to_json(JSON& j, Dev const& x) {
    j = JSON {
        {"name", x.name},
        {"caps", x.caps},
    };
}

void to_json(JSON& j, Vector<Dev> const& x) {
    for (Dev const& dev : x) {
        j[to_string(dev.id)] = dev;
    }
}

} //iod

