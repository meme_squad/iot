#pragma once

#include "devid.hpp"
#include "devcap.hpp"
#include "string.hpp"
#include "vector.hpp"
#include "json.hpp"

namespace iod {

struct Dev {
    DevID          id;
    String         name;
    Vector<DevCap> caps;
};

void to_json(JSON&, Dev const&);
void to_json(JSON&, Vector<Dev> const&);

} //iod
