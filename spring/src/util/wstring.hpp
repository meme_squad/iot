#pragma once

#include "string.hpp"

namespace iod {

using WString = std::wstring;

WString to_wide(String const&);
String from_wide(WString const&);

//inline String to_utf16(String16 const& s) { return s; };
//inline String from_utf16(String const& s) { return s; };

} //iod
