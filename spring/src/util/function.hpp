#pragma once

#include <functional>

namespace iod {

template <typename... Ts>
using Function = std::function<Ts...>;

} //iod
