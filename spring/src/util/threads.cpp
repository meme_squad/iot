#include "threads.hpp"
#include <chrono>
#include <thread>

namespace iod {

void sleep_ms(int ms) {
    std::this_thread::sleep_for(
        std::chrono::milliseconds(ms)
        );
}

} //iod

