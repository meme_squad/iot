#include "color.hpp"

namespace iod {
    
String to_string(Color c) {
	return "\033[" 
		+ to_string(static_cast<int>(c)) 
		+ "m";
}

} //iod

