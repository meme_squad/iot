#pragma once

#include "string.hpp"

namespace iod {

	struct RGB {
		int r = 0;
        int g = 0;
        int b = 0;
        
        RGB() = default;
        RGB(int r_, int g_, int b_)
            : r(r_)
            , g(g_)
            , b(b_)
        {}
	};

	bool operator==(RGB, RGB);
	bool operator!=(RGB, RGB);

	struct HSL {
		float h = 0.0f;
        float s = 0.0f;
        float l = 0.0f;
        
        HSL() = default;
        HSL(float h_, float s_, float l_)
            : h(h_)
            , s(s_)
            , l(l_)
        {}
	};

	bool operator==(HSL, HSL);
	bool operator!=(HSL, HSL);

	RGB hsl_to_rgb(HSL);
	HSL rgb_to_hsl(RGB);
	String to_string(RGB);
	String to_string(HSL);

} // namespace iod
