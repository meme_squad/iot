#pragma once

#include "string.hpp"
#include "result.hpp"
#include <fstream>
#include <sstream>

namespace iod {

class FileLines 
{
    public:
        class Iter 
        {
            public:
                explicit Iter(std::ifstream*);
                
                Iter& operator++();
                bool operator==(Iter const&) const;
                bool operator!=(Iter const&) const;
                std::pair<std::stringstream&, size_t> operator*();

        private:
            bool at_end() const;
            
            std::ifstream* _file = nullptr;
            std::stringstream _stream;
            size_t _num = 0;
            String _line;
        };

    public:
        explicit FileLines(String const&);

        Iter begin();
        Iter end() const;
    
    private:
        std::ifstream _file;
};

inline
FileLines lines_in(String const& path) 
{
    return FileLines(path);
}

Optional<String> read_file(String const&);
bool file_exists(String const&);

} // namespace iod
