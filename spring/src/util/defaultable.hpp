#pragma once

#include <optional>

namespace iod {

template <typename T>
using Defaultable = std::optional<T>;

} // namespace iod
