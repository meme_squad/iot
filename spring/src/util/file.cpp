#include "file.hpp"

#include <filesystem>

namespace fs = std::filesystem;

namespace iod {

FileLines::FileLines(String const& path)
    : _file(path)
    
{}


FileLines::Iter FileLines::begin() 
{
    return Iter(&_file);
}

FileLines::Iter FileLines::end() const 
{
    return Iter(nullptr);
}

FileLines::Iter::Iter(std::ifstream* file)
    : _file(file)
{
    this->operator++();
}

FileLines::Iter& FileLines::Iter::operator++() 
{
    if (!at_end()) 
    {
        getline(*_file, _line);
        _stream = std::stringstream(_line);
        ++_num;
    }
    else
    {
        _line.clear();
        _stream = std::stringstream();
    }

    return *this;
}

bool FileLines::Iter::operator==(Iter const& o) const 
{
    return (at_end() == o.at_end())
        || (_file && o._file && _file->tellg() == o._file->tellg());
}

bool FileLines::Iter::operator!=(Iter const& o) const 
{
    return !(*this == o);
}

std::pair<std::stringstream&, size_t> FileLines::Iter::operator*()
{
    return { _stream, _num };
}

bool FileLines::Iter::at_end() const 
{
    return !_file
        || !(*_file);
}

Optional<String> read_file(String const& path) 
{
    std::ifstream file(path);

    if (!file) 
    {
        return failure;
    }

    String s;
    file.seekg(0, std::ios::end);
    s.reserve(static_cast<size_t>(file.tellg()));
    file.seekg(0, std::ios::beg);

    s.assign((std::istreambuf_iterator<char>(file)),
              std::istreambuf_iterator<char>());

    return s;
}

bool file_exists(String const& path)
{
    return fs::exists(fs::path(path));
}

} // namespace iod LCOV_EXCL_LINE
