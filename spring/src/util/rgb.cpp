#include "rgb.hpp"
#include <math.h>
#include <algorithm>

namespace iod {

bool operator==(RGB a, RGB b) {
	return a.r == b.r
        && a.g == b.g
        && a.b == b.b;
}

bool operator!=(RGB a, RGB b) {
    return !(a == b);
}

bool operator==(HSL a, HSL b) {
    return a.h == b.h
        && a.s == b.s
        && a.l == b.l;
}

bool operator!=(HSL a, HSL b) {
    return !(a == b);
}

static
float qqh_to_rgb(float q1, float q2, float hue) {
    if (hue > 360.0f) {
        hue -= 360.0f;
    }
    else if (hue < 0.0f) {
        hue += 360.0f;
    }

    if (hue < 60.0f) {
        return q1 + (q2 - q1) * hue / 60.0f;
    }
    else if (hue < 180.0f) {
        return q2;
    }
    else if (hue < 240.0f) {
        return q1 + (q2 - q1) * (240.0f - hue) / 60.0f;
    }
    else {
        return q1;
    }
}

RGB hsl_to_rgb(HSL hsl) {
    float p2;
    
    if (hsl.l <= 0.5f) {
        p2 = hsl.l * (1.0f + hsl.s);
    }
    else {
        p2 = hsl.l + hsl.s - hsl.l * hsl.s;
    }

    float p1 = 2.0f * hsl.l - p2;
    float double_r, double_g, double_b;

    if (hsl.s == 0) {
        double_r = hsl.l;
        double_g = hsl.l;
        double_b = hsl.l;
    }
    else {
        double_r = qqh_to_rgb(p1, p2, hsl.h + 120.0f);
        double_g = qqh_to_rgb(p1, p2, hsl.h);
        double_b = qqh_to_rgb(p1, p2, hsl.h - 120.0f);
    }

    return {
        static_cast<int>(double_r * 255.0f),
        static_cast<int>(double_g * 255.0f),
        static_cast<int>(double_b * 255.0f)
    };
}

HSL rgb_to_hsl(RGB rgb) {
    constexpr float epsilon = 0.00001f;

    HSL hsl;
    float r = static_cast<float>(rgb.r) / 255.0f;
    float g = static_cast<float>(rgb.g) / 255.0f;
    float b = static_cast<float>(rgb.b) / 255.0f;

    float max = std::max({r, g, b});
    float min = std::min({r, g, b});
    float diff = max - min;

    hsl.l = (max + min) / 2.0f;

    if (abs(diff) < epsilon) {
        hsl.s = 0;
        hsl.h = 0;
        return hsl;
    }

    if (hsl.l <= 0.5f) {
        hsl.s = diff / (max + min);
    }
    else {
        hsl.s = diff / (2.0f - max - min);
    }

    float r_dist = (max - r) / diff;
    float g_dist = (max - g) / diff;
    float b_dist = (max - b) / diff;

    if (r == max) {
        hsl.h = b_dist - g_dist;
    }
    else if (g == max) {
        hsl.h = 2.0f + r_dist - b_dist;
    }
    else { // b == max
        hsl.h = 4 + g_dist - r_dist;
    }

    hsl.h *= 60.0f;
    
    if (hsl.h < 0) {
        hsl.h += 360.0f;
    }

    return hsl;
}

String to_string(RGB rgb) {
    return "(" 
         + to_string(rgb.r)
         + "r "
         + to_string(rgb.g)
         + "g "
         + to_string(rgb.b)
         + "b)";
}

String to_string(HSL hsl) {
    return "(" 
         + to_string(hsl.h)
         + "h "
         + to_string(hsl.s)
         + "s "
         + to_string(hsl.l)
         + "l)";
}

} // namespace iod

