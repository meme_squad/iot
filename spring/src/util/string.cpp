#include "string.hpp"
#include <boost/locale.hpp>

namespace iod {

using namespace boost::locale::conv;

//TODO these should maybe catch exceptions
WString to_wide(String const& s) {
    return utf_to_utf<wchar_t>(s);
}

String from_wide(WString const& s) {
    return utf_to_utf<char>(s);
}

String to_string(bool b) {
    if (b) {
        return "true";
    }
    else {
        return "false";
    }
}

Optional<String> pop_prefix(String const& s, String const& prefix) {
    if (prefix.empty()) {
        return s;
    }

    size_t end = std::min(s.size(), prefix.size());
    size_t i = 0;

    while (i < end) {
        if (s[i] != prefix[i]) {
            break;
        }

        ++i;

        if (i == prefix.size())
        {
            return s.substr(i, s.size());
        }
    }

    return failure;
}


} //iod
