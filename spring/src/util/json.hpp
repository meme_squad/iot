#pragma once

#include <nlohmann/json.hpp>

namespace iod {

using JSON = nlohmann::json;
using JSONException = nlohmann::detail::exception;

} //iod
