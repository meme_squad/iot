#pragma once

#include "string.hpp"
#include "vector.hpp"

namespace iod {

enum class UrlScheme { http, https };

UrlScheme const http = UrlScheme::http;
UrlScheme const https = UrlScheme::https;

class Url {
    UrlScheme _scheme; 
    String _host;
    Vector<String> _path;

    static char const* scheme_string(UrlScheme scheme) {
        switch (scheme) {
            case UrlScheme::http: return "http://";
            case UrlScheme::https: return "https://";
        }

        std::abort();
    }

    void normalize() {
        Vector<String> all;
        String* cur = nullptr;

        auto next = [&]() {
            all.push_back("");
            cur = &all.back();
        };

        auto norm = [&](String const& s) {
            for (char c : s) {
                if (c == '/') {
                    next();
                }
                else {
                    cur->push_back(c);
                }
            }
        };

        next();
        norm(_host);

        for (String const& piece : _path) {
            next();
            norm(piece);
        }

        _host.clear();
        _path.clear();
   
        auto empty = [](String const& s) {
            return s.empty();
        };

		erase_if(all, empty);

        if (all.empty()) {
            return;
        }
    
        auto it = all.begin();
        _host = std::move(*it);
        ++it;

        while (it != all.end()) {
            _path.push_back(std::move(*it));
            ++it;
        }
    }

    public:
        Url(UrlScheme scheme)
            : _scheme(scheme)
        {}

        Url& operator /=(char const* piece) {
            if (_host.empty()) {
                _host = piece;
            }
            else {
                _path.push_back(piece);
            }
            normalize();
            return *this;
        }
        
        Url& operator /=(String&& piece) {
            if (_host.empty()) {
                _host = std::move(piece);
            }
            else {
                _path.push_back(std::move(piece));
            }
            normalize();
            return *this;
        }
        
        Url& operator /=(String const& piece) {
            if (_host.empty()) {
                _host = piece;
            }
            else {
                _path.push_back(piece);
            }
            normalize();
            return *this;
        }
        
        String to_string() const {
            String result = scheme_string(_scheme) + _host;

            for (String const& piece : _path) {
                result += "/" + piece;
            }

            return result;
        }

        UrlScheme scheme() const {
            return _scheme;
        }

        String const& host() const {
            return _host;
        }

        Vector<String> const& path() const {
            return _path;
        }
};

inline
String to_string(Url const& url) {
    return url.to_string();
}

inline
Url operator/(UrlScheme scheme, char const* path) { 
    Url url(scheme);
    url /= path;
    return url;
}

inline
Url operator/(UrlScheme scheme, String&& path) { 
    Url url(scheme);
    url /= std::move(path);
    return url;
}

inline
Url operator/(UrlScheme scheme, String const& path) { 
    Url url(scheme);
    url /= path;
    return url;
}

inline
Url operator/(Url&& url, char const* path) { 
    url /= path;
    return url;
}

inline
Url operator/(Url&& url, String&& path) { 
    url /= std::move(path);
    return url;
}

inline
Url operator/(Url&& url, String const& path) { 
    url /= path;
    return url;
}

} // namespace iod
