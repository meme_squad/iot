#pragma once

#include "string.hpp"
#include "future.hpp"
#include "color.hpp"

namespace iod {

inline Mutex output_mutex;

namespace detail {
    void _safe_print(String const&);
    void _safe_print(WString const&);
    void _safe_print(char const*);
    void _safe_print(wchar_t const*);
    void _flush();
    String _timestamp();

    template <typename T>
    void _safe_print(T const& t) {
        _safe_print(to_string(t));
    }
} // namespace iod::detail

template <typename... Ts>
void safe_print(Ts&&... ts) {
    auto _ = LockGuard(output_mutex);
	(detail::_safe_print(std::forward<Ts>(ts)), ...);
    detail::_flush();
}

#define PRINT(color, ...) safe_print(Color::color, detail::_timestamp(), " [", MODULE_ID, "] ", __VA_ARGS__, Color::off, "\n")

#define PRINT_NOTICE(...) PRINT(MODULE_COLOR, __func__, ": ", __VA_ARGS__)
#define PRINT_ERROR(...)  PRINT(red, __func__, ": ", __VA_ARGS__)
#define PRINT_TRACE       PRINT(gray, __func__)

// TRY that outputs on failure.
#define $TRY(x,y) TRY(x, y ^ []{ PRINT_ERROR("TRY failed at " #y); })
#define $TRY_(y) TRY_(y    ^ []{ PRINT_ERROR("TRY failed at " #y); })

} //iod
