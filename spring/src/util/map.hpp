#pragma once

#include "result.hpp"
#include "memory.hpp"
#include <unordered_map>
#include <utility>

namespace iod {

template <typename K, typename V>
using Map = std::unordered_map<K,V>;

template <typename K, typename V>
Optional<V> lookup(Map<K,V> const& map, K const& key) {
    auto it = map.find(key);

    // Have to be explicit here. Optional<any> didn't work.
    if (it == map.end()) {
        return Optional<V>(outcome::in_place_type_t<Fail>());
    }
    else {
        return Optional<V>(outcome::in_place_type_t<V>(), it->second);
    }
}

template <typename K, typename V>
Optional<V*> lookup(Map<K,UniquePtr<V>>& map, K const& key) {
    auto it = map.find(key);

    if (it == map.end()) {
        return failure;
    }
    else {
        return it->second.get();
    }
}

template <typename K, typename V, typename F>
void erase_if(Map<K,V> map, F const& pred) {
    for (auto it = map.begin(), end = map.end(); it != end; ) {
        if (pred(*it)) {
            it = map.erase(it);
        } 
        else {
            ++it;
        }
    }
}

} //iod
