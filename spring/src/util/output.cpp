#include "output.hpp"
#include "config.hpp"
#include <iostream>
#include <chrono>
#include <sstream>
#include <iomanip>

namespace iod::detail {

using namespace std;

// Returns HH:MM:SS current time.
String _timestamp() {
    using namespace chrono;
    time_t const now = system_clock::to_time_t(system_clock::now());
    stringstream s;
    s << put_time(localtime(&now), "%T");
    return s.str();
}

void _flush() {
#ifdef _WIN32
    wcout << flush;
#else
    cout << flush;
#endif
}

void _safe_print(char const* x) {
#ifdef _WIN32
    wcout << x;
#else
    cout << x;
#endif
}

void _safe_print(wchar_t const* x) {
#ifdef _WIN32
    wcout << x;
#else
    cout << x;
#endif
}

void _safe_print(string const& x) {
#ifdef _WIN32
    wcout << to_wide(x);
#else
    cout << x;
#endif
}

void _safe_print(wstring const& x) {
#ifdef _WIN32
    wcout << x;
#else
    cout << from_wide(x);
#endif
}

} //iod::detail
