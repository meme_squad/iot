#pragma once

#include <future>
#include <mutex>

namespace iod {

template <typename T>
using Future = std::future<T>;

template <typename T>
using PackagedTask = std::packaged_task<T>;

using std::async;
using std::launch;

using Mutex = std::mutex;

using LockGuard = std::lock_guard<Mutex>;

} //iod
