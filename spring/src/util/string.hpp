#pragma once

#include "result.hpp"
#include <string>

namespace iod {

using String = std::string;
using WString = std::wstring;
using FilePath = String;

using std::to_string;

WString to_wide(String const&);
String from_wide(WString const&);
inline WString to_wide(WString const& s) { return s; }
inline String from_wide(String const& s) { return s; }
String to_string(bool);
Optional<String> pop_prefix(String const& s, String const& prefix);

inline auto to_rest_string(String const& s) {
#ifdef _WIN32
    return to_wide(s);
#else
	return s;
#endif
}


} //iod
