#include "app.hpp"
#include <iostream>
#include <stdlib.h>

int main(int, char**) {
#ifndef _WIN32
    setenv("SSL_CERT_DIR","/etc/ssl/certs",1);
    std::cout << ":)";
#endif

    try {
        iod::app();
    }
    catch(std::exception const& e) {
        std::cout << "Exception: " << e.what() << std::endl;
    }
    
    return 0;
}
