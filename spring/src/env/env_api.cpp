#include "env_api.hpp"
#include "env_impl.hpp"

namespace iod {

using QueueTask = Env::QueueTask;
using APITask   = Env::APITask;

void push_task(Env* env, QueueTask&& qt) {
    LockGuard lock(env->queue_mutex);
    env->task_queue.push_back(std::move(qt));
}

void push_tasks(Env* env, Vector<QueueTask>&& tasks) {
    LockGuard lock(env->queue_mutex);
    move_append(env->task_queue, std::move(tasks));
}

APIResult<APIFuture> queue_task(
    Env* env, 
    HardwareID const& hw_id, 
    APICall&& func
    ) {
    Optional<Hardware*> hw = lookup(env->hardware_map, hw_id);

    if (!hw) {
        return APIError::hardware_not_found;
    }

    APITask task([func](Hardware* hw_) {
		return func(hw_);
		});
    APIFuture fut(task.get_future());
    push_task(env, QueueTask{ *hw, std::move(task) });
    return std::move(fut);
}

APIResult<Vector<APIFuture>> queue_mass_task(
    Env* env, 
    APICall const& func) {
    Vector<QueueTask> tasks;
    Vector<APIFuture> futs;
    
    for (auto& [hw_id, hw] : env->hardware_map) {
        UNUSED(hw_id);
		APITask task([func](Hardware* hw_) {
			return func(hw_);
			});
        APIFuture fut(task.get_future());
        tasks.push_back(QueueTask{ hw.get(), std::move(task) });
        futs.push_back(std::move(fut));
    }

    push_tasks(env, std::move(tasks)); 
    return std::move(futs);
}

} //iod
