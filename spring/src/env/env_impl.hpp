// env_impl.hpp
//  The actual Env type.
#pragma once

#include "apitask.hpp"
#include "hardware.hpp"
#include "future.hpp"
#include "map.hpp"
#include "memory.hpp"
#include "string.hpp"
#include "defaultable.hpp"
#include "vector.hpp"

namespace iod {

// Env is the app's shared state.
// It can only be accessed through env_api.hpp or env_app.hpp.
// Its concurrent properties are documented below.
struct Env {
    using APITaskSlot = Future<void>;

    using APITask = PackagedTask<
        Defaultable<APIResult<Vector<Dev>>>(Hardware*)
        >;
    
    struct QueueTask {
        Hardware* hw;
        APITask   task;
    };

    // hardware is touched by:
    //  add_hardware() on the app thread (before server start)
    //  queue_task() and queue_mass_task() on each api thread (read only)
    // (no synchronization needed)
    Map<HardwareID, UniquePtr<Hardware>> hardware_map;

    // task_slots is touched by:
    //  env_loop() on the app thread
    // (no synchronization needed)
    Map<Hardware*, APITaskSlot> task_slots;
    
    // task_queue is touched by:
    //  pop_task() on the app thread
    //  push_task() and push_tasks() on each api thread
    // (synchronized through queue_mutex)
    Vector<QueueTask> task_queue; //TODO better data structure?
    Mutex queue_mutex;
};

} //iod
