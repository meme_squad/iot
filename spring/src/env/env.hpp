// env.hpp
//  This only exports the opaque type.
//  It saves compile time and keeps things on a need-to-know basis 
//    because there's concurrent stuff going on.
#pragma once

namespace iod {

struct Env;

} //iod
