// env_api.hpp
//  This is the interface for Env to api.cpp.
#pragma once

#include "env.hpp"
#include "devid.hpp"
#include "apitask.hpp"
#include "result.hpp"
#include "apiresult.hpp"

namespace iod {

APIResult<APIFuture> queue_task(Env*, HardwareID const&, APICall&&);
APIResult<Vector<APIFuture>> queue_mass_task(Env*, APICall const&);

} //iod
