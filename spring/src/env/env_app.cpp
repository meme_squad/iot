#include "env_app.hpp"
#include "env_impl.hpp"
#include "threads.hpp"
#include "output.hpp"

#define MODULE_ID "ENV"
#define MODULE_COLOR blue

namespace iod {

using APITask = Env::APITask;
using APITaskSlot = Env::APITaskSlot;

OpaquePtr<Env> new_env() {
    return OpaquePtr<Env>(new Env);
}

void free_opaque_ptr(Env* env) {
    delete env;
}

void add_hardware(Env* env, UniquePtr<Hardware> hw_ptr) {
    HardwareID hw_id = hw_ptr->hw_id();
    UniquePtr<Hardware>& hw = env->hardware_map[hw_id];
    hw = std::move(hw_ptr);
	hw->init_database(); 
	hw->load_config();
    env->task_slots[hw.get()] = {};
    PRINT_NOTICE("loaded ", hw->hw_id());
}

// Maybe returns the next task keyed to this Hardware.
Optional<APITask> pop_task(Env* env, Hardware* hw) {
    LockGuard lock(env->queue_mutex);
    auto& tasks = env->task_queue;

    if (tasks.empty()) {
        return failure;
    }

    for (auto it = tasks.begin(); it != tasks.end(); ++it) {
        if (it->hw == hw) {
            APITask task = std::move(it->task);
            tasks.erase(it);
            return std::move(task);
        }
    }
    
    return failure;
}

bool slot_done(APITaskSlot& slot) {
    return slot.wait_for(std::chrono::seconds(0)) 
        == std::future_status::ready;
}

void update_slot(Env* env, Hardware* hw, APITaskSlot& slot) {
    auto try_to_slot_next_task = [&]() {
        if (Optional<APITask> task = pop_task(env, hw)) {
            slot = async(launch::async, std::move(*task), hw);
            PRINT_NOTICE("slotted task into ", hw->hw_id());
            return true;
        }
        else {
            return false;
        }
    };

    if (!slot.valid()) {
        // Slot empty.
        try_to_slot_next_task();
    }
    else if (slot_done(slot)) {
        // Slot done.
        if (!try_to_slot_next_task()) {
            slot = {}; // Make empty.
        }
    }
    else {
        // Slot still working.
    }
}

void env_loop(Env* env) {
    size_t n = 0;

    while (true) {
        if (n != env->task_queue.size()) {
            n = env->task_queue.size();
            PRINT_NOTICE("queue size now ", n);
        }

        for (auto& [hw, slot] : env->task_slots) {
            update_slot(env, hw, slot);
        }

        sleep_ms(16); // TODO config
    }
}

} //iod
