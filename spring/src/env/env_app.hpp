// env_app.hpp
//  This is the interface for Env to app.cpp.
#pragma once

#include "env.hpp"
#include "memory.hpp"
#include "hardware.hpp"

namespace iod {

OpaquePtr<Env> new_env();
void free_opaque_ptr(Env*);
void env_loop(Env*);
void add_hardware(Env*, UniquePtr<Hardware>);

template <typename T>
void add_hardware(Env* env) {
    add_hardware(env, make_unique<T>());
}

} //iod
