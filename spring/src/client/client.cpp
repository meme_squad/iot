#include "client.hpp"
#include "output.hpp"
#include "config.hpp"
#include <cpprest/http_client.h>

#define MODULE_ID "CLIENT"
#define MODULE_COLOR yellow

namespace iod {

namespace rest {
    using namespace web::http;
    using namespace web::http::client;
    using pplx::task;
}

static
Optional<String> http_req(
    Url const& url, 
    rest::method const& mthd, 
    String const& payload
    ) {
    String const url_string = to_string(url);
    PRINT_NOTICE(mthd, " ", url_string);
    rest::http_client client(to_rest_string(url_string));

    rest::task req = 
        client.request(mthd, "/", payload)
        .then([=](rest::http_response response) {
            PRINT_NOTICE("got status code ", response.status_code());
            return response.extract_string();
        });

    try {
        return from_wide(req.get());
    }
    catch (std::exception const& e) {
        PRINT_ERROR(e.what(), " in ", url_string);
        return failure;
//        throw ClientException(e.what());
    }
}

Optional<String> http_get(Url const& url) {
    return http_req(url, rest::methods::GET, "");
}

Optional<String> http_put(Url const& url, String const& payload) {
    return http_req(url, rest::methods::PUT, payload);
}

Optional<String> http_post(Url const& url, String const& payload) {
    return http_req(url, rest::methods::POST, payload);
}

} //iod
