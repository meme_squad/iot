#pragma once

#include "string.hpp"
#include "result.hpp"
#include "exception.hpp"
#include "url.hpp"

namespace iod {

//TODO decide on exceptions or not
//using ClientException = RuntimeError<struct ClientException_>;

Optional<String> http_get(Url const&);
Optional<String> http_put(Url const&, String const&);
Optional<String> http_post(Url const&, String const&);

} //iod
