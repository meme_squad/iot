#pragma once

#include "string.hpp"

namespace iod::config {

String const app_cfg_path = "iod.cfg";

} //iod::config
